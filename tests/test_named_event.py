import pytest
from s3i.event_system import NamedEvent
from uuid import uuid4

def test_generate_event_msg():
    """Ensures event message are properly build and filled."""
    event = NamedEvent(f"s3i://{uuid4()}.moisture", {})
    event.generate_event_msg({})