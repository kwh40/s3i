import os

import pytest
from dotenv import load_dotenv

from s3i.directory import Directory
from s3i.identity_provider import IdentityProvider, TokenType

load_dotenv("tests/integration/.env")


@pytest.fixture
def username():
    return os.getenv("username")


@pytest.fixture
def password():
    return os.getenv("password")


@pytest.fixture
def thing_id():
    return os.getenv("thing_id")


@pytest.fixture
def token(username, password):
    idp = IdentityProvider(
        grant_type="password",
        client_id="admin-cli",
        client_secret="",
        username=username,
        password=password,
    )
    return idp.get_token(TokenType.ACCESS_TOKEN)


@pytest.fixture
def directory(token, directory_url):
    return Directory(directory_url, token)


# TODO: Activate test: https://git.rwth-aachen.de/kwh40/s3i/-/issues/26  
# def test_queryEndpointService(directory: Directory, thing_id):
#     res = directory.queryEndpointService(thing_id, "push")
#     assert len(res) == 0
