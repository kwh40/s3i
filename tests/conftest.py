import pytest


@pytest.fixture
def directory_url():
    return "https://dir.s3i.vswf.dev/api/2/"


@pytest.fixture
def jwt_access_token():
    return "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJPbzRfUTE0UGxjeVJfX1A5MXAtYXRaWURYa29GVV9ORk5qLWtLdDVHdXNJIn0.eyJleHAiOjE2Njc2OTE5OTYsImlhdCI6MTY2NzY3Mzk5NiwianRpIjoiMjY0ZjI3ODEtNjk3My00NGE4LTliMWMtNWFlZjgyNDBlZDQwIiwiaXNzIjoiaHR0cHM6Ly9pZHAuczNpLnZzd2YuZGV2L2F1dGgvcmVhbG1zL0tXSCIsImF1ZCI6InJhYmJpdG1xIiwic3ViIjoiZDZlZWI2ZDItNGY2ZS00ODg0LWEyNTAtMmQ5ZGQ4YWQ2N2M2IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiYWRtaW4tY2xpIiwic2Vzc2lvbl9zdGF0ZSI6IjljODhkMTdjLWYzOTQtNGRmNS1iNzgyLWE1M2ZmYzUwMTFkNSIsImFjciI6IjEiLCJzY29wZSI6InJhYmJpdG1xLmNvbmZpZ3VyZToqLyovKiByYWJiaXRtcS50YWc6YWRtaW5pc3RyYXRvciByYWJiaXRtcS53cml0ZToqLyovKiBwcm9maWxlIHJhYmJpdG1xLnJlYWQ6Ki8qLyogZW1haWwiLCJzaWQiOiI5Yzg4ZDE3Yy1mMzk0LTRkZjUtYjc4Mi1hNTNmZmM1MDExZDUiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwicHJlZmVycmVkX3VzZXJuYW1lIjoiYWxicmVjaHQiLCJlbWFpbCI6ImNocmlzdGlhbi5hbGJyZWNodEBzdHVkZW50LnJ0LnJpZi1ldi5kZSJ9.W0OvZ4KjqyY6lT7lpbMPX0qW2sQ0T_MZIelK3rsTNTyr0EyGjhnb-pFnDzoP-JBd2CYGjYF5ughU9f-fqVtzYG3j8HBDk5TaUJxV-SSSyq7WBaf0fgAXIN1R7qRFt_Aqu2idxy0tBWJ8N6fbeCeqmNJdb0CFygOScMjgo2CGOTuqOmihfqJd1kVdqjCiMGKhfW_mQ7ZlkA9Oi-DDPXkAR-tf_R_AoVIT-FGr2nW5gAJ4AcPgqr6G6FK4YTND-ZVGiF0pMnHiLpfMpaBMQAEL6m9YPSDETPm-8-X43DWkg4FcFbz1wFqMNrzqM5PhfI_Ld3LpfRL4xZ1s1i2syR1ncw"


@pytest.fixture
def token_bundle_valid():
    return {
        "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJPbzRfUTE0UGxjeVJfX1A5MXAtYXRaWURYa29GVV9ORk5qLWtLdDVHdXNJIn0.eyJleHAiOjE2Njc2OTM4NTAsImlhdCI6MTY2NzY3NTg1MCwianRpIjoiM2UwYjcwMjUtYmM3My00YzI1LTgxMTctZmFiMzM5ZGNiNDFjIiwiaXNzIjoiaHR0cHM6Ly9pZHAuczNpLnZzd2YuZGV2L2F1dGgvcmVhbG1zL0tXSCIsImF1ZCI6InJhYmJpdG1xIiwic3ViIjoiZDZlZWI2ZDItNGY2ZS00ODg0LWEyNTAtMmQ5ZGQ4YWQ2N2M2IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiYWRtaW4tY2xpIiwic2Vzc2lvbl9zdGF0ZSI6Ijg4ZmJlYzllLWVhMmQtNDkxOS05YmJmLTJjOTNjZDRmN2VjNyIsImFjciI6IjEiLCJzY29wZSI6InJhYmJpdG1xLmNvbmZpZ3VyZToqLyovKiByYWJiaXRtcS50YWc6YWRtaW5pc3RyYXRvciByYWJiaXRtcS53cml0ZToqLyovKiBwcm9maWxlIHJhYmJpdG1xLnJlYWQ6Ki8qLyogZW1haWwiLCJzaWQiOiI4OGZiZWM5ZS1lYTJkLTQ5MTktOWJiZi0yYzkzY2Q0ZjdlYzciLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwicHJlZmVycmVkX3VzZXJuYW1lIjoiYWxicmVjaHQiLCJlbWFpbCI6ImNocmlzdGlhbi5hbGJyZWNodEBzdHVkZW50LnJ0LnJpZi1ldi5kZSJ9.LKPbJXfSNMoYkkSGEiOlbQO1uOPGRoqlWw2LYDs1h1INeYshUTcQX9KCkvLkmIW8tVjY9z3fheAFXCWFoTifYDXR3MBdPd8pLSz7UMIZ6QmkAZwFYdi8zn45gZpIeeG7ktHAhxuAy-6MAGzbg9pkaYe-RBvjXBp7iFe4sc_33-6KBKC2VUHOgtKx71NpE4Kf0vvr2i-mDjKOU8ylFFgf7H1iMNE8iAQziigFv0OYgY2zpKS_j3XWKikuY5S4qtd8gi0vqJT0YKa8ohyCpoHwKHHHlDKoxiFklm4qzmQm_D9fqkkzhuLu5NJF1Cdy8PC3RLX0WjYIJKxK8bevhnvjNg",
        "expires_in": 18000,
        "refresh_expires_in": 10800,
        "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJhM2Y1NjhhYS0yMjlkLTQyZWItYmZiMC04ZDc3YzhjMzFkMzUifQ.eyJleHAiOjE2Njc2ODY2NTAsImlhdCI6MTY2NzY3NTg1MCwianRpIjoiYTRmOWI5YTUtZmQ3Yy00MWQ5LTliYTgtMGM5YTdlMzJlOTA0IiwiaXNzIjoiaHR0cHM6Ly9pZHAuczNpLnZzd2YuZGV2L2F1dGgvcmVhbG1zL0tXSCIsImF1ZCI6Imh0dHBzOi8vaWRwLnMzaS52c3dmLmRldi9hdXRoL3JlYWxtcy9LV0giLCJzdWIiOiJkNmVlYjZkMi00ZjZlLTQ4ODQtYTI1MC0yZDlkZDhhZDY3YzYiLCJ0eXAiOiJSZWZyZXNoIiwiYXpwIjoiYWRtaW4tY2xpIiwic2Vzc2lvbl9zdGF0ZSI6Ijg4ZmJlYzllLWVhMmQtNDkxOS05YmJmLTJjOTNjZDRmN2VjNyIsInNjb3BlIjoicmFiYml0bXEuY29uZmlndXJlOiovKi8qIHJhYmJpdG1xLnRhZzphZG1pbmlzdHJhdG9yIHJhYmJpdG1xLndyaXRlOiovKi8qIHByb2ZpbGUgcmFiYml0bXEucmVhZDoqLyovKiBlbWFpbCIsInNpZCI6Ijg4ZmJlYzllLWVhMmQtNDkxOS05YmJmLTJjOTNjZDRmN2VjNyJ9.tgK2B404rPuX0ElH6jm-SZiogo9u290i1_Ld4A31vtM",
        "token_type": "Bearer",
        "not-before-policy": 0,
        "session_state": "88fbec9e-ea2d-4919-9bbf-2c93cd4f7ec7",
        "scope": "rabbitmq.configure:*/*/* rabbitmq.tag:administrator rabbitmq.write:*/*/* profile rabbitmq.read:*/*/* email",
    }


@pytest.fixture
def cert_idp_json():
    return {
        "keys": [
            {
                "kid": "Oo4_Q14PlcyR__P91p-atZYDXkoFU_NFNj-kKt5GusI",
                "kty": "RSA",
                "alg": "RS256",
                "use": "sig",
                "n": "ohccYxv1zMiLLwt9cgXk_y7H3PsJ-_e1ZkMyD0qzvK4qhdVfIAGIzGB7SirMGAzrN6_jG7BFDdAGmucFBiW49PBOhDOstYiT-atYcPHzgUhrA6Ox_ofOAmXB6dAP6XBZzYzAcE2uZgXOdRFenMNwaAblQKaum8S1CgsSRyoruodnaDXPBxR4nKAoCOnzFrxlQpXjwct4__5-SD7WhiCcf7IjKej2gIcLmSBpD1iuiU4ZdbbUZLVfcmypvQ-Hbs9biICKyK06dfgqFWbqkzTaq310_8gaqyeP7EiiA_-aQcW0RxIDqgGgV0tkQ8UZFrUX2Ac5dkhVgK9cw4kUCIB0pw",
                "e": "AQAB",
                "x5c": [
                    "MIIClTCCAX0CBgFu/5zL5DANBgkqhkiG9w0BAQsFADAOMQwwCgYDVQQDDANLV0gwHhcNMTkxMjEzMTQxMzE2WhcNMjkxMjEzMTQxNDU2WjAOMQwwCgYDVQQDDANLV0gwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCiFxxjG/XMyIsvC31yBeT/Lsfc+wn797VmQzIPSrO8riqF1V8gAYjMYHtKKswYDOs3r+MbsEUN0Aaa5wUGJbj08E6EM6y1iJP5q1hw8fOBSGsDo7H+h84CZcHp0A/pcFnNjMBwTa5mBc51EV6cw3BoBuVApq6bxLUKCxJHKiu6h2doNc8HFHicoCgI6fMWvGVClePBy3j//n5IPtaGIJx/siMp6PaAhwuZIGkPWK6JThl1ttRktV9ybKm9D4duz1uIgIrIrTp1+CoVZuqTNNqrfXT/yBqrJ4/sSKID/5pBxbRHEgOqAaBXS2RDxRkWtRfYBzl2SFWAr1zDiRQIgHSnAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAFJodZ3cDdp9mmJY9bkgw+0DbYBMHsq6sHXvIKGh816dI+65ZRmVZvpL7aSPXHox6JY4W/oyh5cgp/KLzVR8m+TmEjpy5UmEFJ65rF2jJYelCCvQ4WegVejPH4yw5+2C4Ndk3hEsY9uIb5Ehd6NnJ4xxKgJQzq6jX0JsCaHuMTDRagw5w3RUCfAca75ELq48loLKXc74jp5Z62Rrw4al1D7DurswgJ1QnOpm0v4TRkBPbt1Os/jmbiRjOOyqqOoxwzW78AMv9bLK+N0clfN5lnrhqSAm/81w70QjxgjgqQvYEp8CGSikHJShGpIcMleZ26rQvjdBam2XaQlLhCbAOGU="
                ],
                "x5t": "2qmCCYwxlUWjiEMWcaTf970SYd4",
                "x5t#S256": "vsnuOgChjjx_5L4j-eCz-LkSWmvZ24iZQtaf_ObUoP8",
            }
        ]
    }
