import asyncio

import pytest
import pytest_asyncio

from s3i.callback_manager import CallbackManager


@pytest.mark.asyncio
async def async_callback():
    return 10


def callback():
    return 10


@pytest.fixture
def prefix():
    return "prefix"


@pytest.fixture
def manager(prefix):
    manager = CallbackManager()
    assert len(manager._stack) == 0
    manager.add(prefix, callback, False, False)
    assert len(manager._stack) == 1
    return manager


@pytest.fixture
def manager_oa(prefix):
    manager = CallbackManager()
    assert len(manager._stack) == 0
    manager.add(prefix, async_callback, True, True)
    assert len(manager._stack) == 1
    return manager


def test_remove(manager: CallbackManager, prefix: str):
    callback_dict = manager.create_callback_dict(callback, False, False)
    assert manager.remove(prefix, callback_dict) == True
    # Remove it a second time
    assert manager.remove(prefix, callback_dict) == False


def test_clear(manager: CallbackManager):
    assert len(manager._stack) == 1
    manager.clear()
    assert len(manager._stack) == 0


def test_process(manager: CallbackManager, prefix: str):
    assert len(manager._stack) == 1
    assert manager.process(prefix) == True
    # NOTE: If the list assigened to a prefix it is not removed from the "stack".
    assert len(manager._stack) == 1


@pytest.mark.asyncio
async def test_process_oa(manager_oa: CallbackManager, prefix: str, event_loop):
    assert len(manager_oa._stack) == 1
    assert manager_oa.process(prefix, event_loop) == True
    # NOTE: If the list assigened to a prefix it is not removed from the "stack".
    assert len(manager_oa._stack) == 1
