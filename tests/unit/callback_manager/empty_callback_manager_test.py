from ast import Call

import pytest

from s3i.callback_manager import CallbackManager


@pytest.fixture
def manager():
    return CallbackManager()


def callback():
    pass


def test_constructor(manager: CallbackManager):
    assert len(manager._stack) == 0


def test_add(manager: CallbackManager):
    prefix = "prefix"
    assert len(manager._stack) == 0
    # Add a callback succefully
    assert manager.add("prefix", callback, True, True) == prefix
    assert len(manager._stack) == 1
    # Add a duplicate
    assert manager.add("prefix", callback, True, True) == prefix
    assert len(manager._stack) == 1

    # Invalid type for one_shot
    with pytest.raises(TypeError):
        manager.add("prefix", callback, 1, True)

    # Invalid type for is_async
    with pytest.raises(TypeError):
        manager.add("prefix", callback, True, 1)


def test_remove(manager: CallbackManager):
    assert len(manager._stack) == 0
    manager.remove("pre", "blub")
    assert len(manager._stack) == 0


def test_clear(manager: CallbackManager):
    assert len(manager._stack) == 0
    manager.clear()
    assert len(manager._stack) == 0


def test_process(manager: CallbackManager):
    assert manager.process("prefix") == False
