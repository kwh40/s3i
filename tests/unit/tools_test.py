import pytest
from pytest_mock import MockerFixture

from s3i.tools import http_error_from_request_response, query_all, requests


@query_all
def func():
    return "https://kwh40.de", {"Conent-Type": "xml"}

def test_query_all(mocker):
    res_mock_first = mocker.Mock(wraps=requests.Response)
    res_mock_first.status_code = 200
    item = {"thindid": "s3i:abcde"}
    res_json_first = {"items": [item], "cursor": "next_page"}
    res_mock_first.json.return_value = res_json_first

    res_mock_second = mocker.Mock(wraps=requests.Response)
    res_mock_second.status_code = 200
    res_json_second = {"items": []}
    res_mock_second.json.return_value = res_json_second

    mocker.patch.object(
        requests, "get", autospec=True, side_effect=[res_mock_first, res_mock_second]
    )

    res = func()
    assert res == [item]


def test_http_error_from_request_response(mocker):
    status_code = 400
    text = "error"
    headers = {"Content-Type": "html"}
    url = "https://kwh40.de"

    mock_res = mocker.Mock(wraps=requests.Response)
    mock_res.status_code = status_code
    mock_res.text = text
    mock_res.headers = headers
    mock_res.url = url
    res = http_error_from_request_response(mock_res)
    assert res.code == status_code
    assert res.msg == text
    assert res.hdrs == headers

    # TODO: check why KeyError is thrown
    with pytest.raises(KeyError):
        assert res.url == url
