from s3i.repository import Repository


def test_constructor():
    url = "https://repository.de"
    token = "token"
    repository = Repository(url, token)
    assert repository.ditto_url == url
    assert repository.token == token
