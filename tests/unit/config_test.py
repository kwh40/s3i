from urllib import request

import pytest
from pytest_mock import MockerFixture

from s3i.config import Config, requests


@pytest.fixture
def token():
    return "abcd"


@pytest.fixture
def headers(token):
    return {"Content-Type": "application/json", "Authorization": f"Bearer {token}"}


@pytest.fixture
def server_url():
    return "https://config.s3i.vswf.dev"


@pytest.fixture
def config(token):
    return Config(token)


def assert_config_state(config, token, headers, server_url):
    assert config.token == token
    assert config.server_url == server_url
    assert config.headers == headers


def test_constructor(config, token, headers, server_url):
    assert_config_state(config, token, headers, server_url)


def test_set_token(config: Config, headers, server_url):
    new_token = "efgh"
    config.token = new_token
    assert_config_state(config, new_token, headers, server_url)


def test_set_headers(config: Config, token, server_url):
    new_headers = "headers"
    config.headers = new_headers
    assert_config_state(config, token, new_headers, server_url)


def test_set_url(config: Config, token, headers):
    new_url = "https://google.com"
    config.server_url = new_url
    assert_config_state(config, token, headers, new_url)


def test_create_person(mocker: MockerFixture, config: Config):
    res_mock = mocker.Mock(wraps=requests.Response)
    res_mock.status_code = 201
    res_json = {
        "personIdentity": {"username": "root", "identifier": "s3i:abcd"},
        "thingIdentity": {"identifier": "s3i:defg", "secret": "xyz"},
    }
    res_mock.json.return_value = res_json
    mocker.patch.object(requests, "post", autospec=True, return_value=res_mock)

    res = config.create_person("root", "toor")
    assert res == res_mock


def test_delete_person(mocker: MockerFixture, config: Config):
    res_mock = mocker.Mock(wraps=requests.Response)
    res_mock.status_code = 204
    mocker.patch.object(requests, "delete", autospec=True, return_value=res_mock)
    res = config.delete_person("root")
    assert res == res_mock


def test_create_thing(mocker: MockerFixture, config: Config):
    res_mock = mocker.Mock(wraps=requests.Response)
    res_mock.status_code = 201
    res_json = {"identifier": "s3i:abcd", "secret": "xyz"}
    res_mock.json.return_value = res_json
    mocker.patch.object(requests, "post", autospec=True, return_value=res_mock)
    res = config.create_thing()
    assert res == res_mock


def test_create_thing_with_body(mocker: MockerFixture, config: Config):
    res_mock = mocker.Mock(wraps=requests.Response)
    res_mock.status_code = 201
    res_json = {"identifier": "s3i:abcd", "secret": "xyz"}
    res_mock.json.return_value = res_json
    mocker.patch.object(requests, "post", autospec=True, return_value=res_mock)
    res = config.create_thing("body")
    assert res == res_mock


def test_delete_thing(mocker: MockerFixture, config: Config):
    res_mock = mocker.Mock(wraps=requests.Response)
    res_mock.status_code = 204
    mocker.patch.object(requests, "delete", autospec=True, return_value=res_mock)
    res = config.delete_thing("root")
    assert res == res_mock


def test_create_cloud_copy(mocker: MockerFixture, config: Config):
    res_mock = mocker.Mock(wraps=requests.Response)
    res_mock.status_code = 201
    res_json = {"identifier": "s3i:abcd", "secret": "xyz"}
    res_mock.json.return_value = res_json
    mocker.patch.object(requests, "post", autospec=True, return_value=res_mock)
    res = config.create_cloud_copy("s3i:abcd")
    assert res == res_mock


def test_delete_cloud_copy(mocker: MockerFixture, config: Config):
    res_mock = mocker.Mock(wraps=requests.Response)
    res_mock.status_code = 204
    mocker.patch.object(requests, "delete", autospec=True, return_value=res_mock)
    res = config.delete_cloud_copy("s3i:abcd")
    assert res == res_mock


def test_create_broker_queue(mocker: MockerFixture, config: Config):
    res_mock = mocker.Mock(wraps=requests.Response)
    res_mock.status_code = 201
    res_json = {"encrypted": "true"}
    res_mock.json.return_value = res_json
    mocker.patch.object(requests, "post", autospec=True, return_value=res_mock)
    res = config.create_broker_queue("s3i:abcd")
    assert res == res_mock


def test_create_broker_queue__encrypted(mocker: MockerFixture, config: Config):
    res_mock = mocker.Mock(wraps=requests.Response)
    res_mock.status_code = 201
    res_json = {"encrypted": "true"}
    res_mock.json.return_value = res_json
    mocker.patch.object(requests, "post", autospec=True, return_value=res_mock)
    res = config.create_broker_queue("s3i:abcd", True)
    assert res == res_mock


def test_create_broker_queue__plain(mocker: MockerFixture, config: Config):
    res_mock = mocker.Mock(wraps=requests.Response)
    res_mock.status_code = 201
    res_json = {"encrypted": "false"}
    res_mock.json.return_value = res_json
    mocker.patch.object(requests, "post", autospec=True, return_value=res_mock)
    res = config.create_broker_queue("s3i:abcd", False)
    assert res == res_mock


def test_delete_broker_queue(mocker: MockerFixture, config: Config):
    res_mock = mocker.Mock(wraps=requests.Response)
    res_mock.status_code = 204
    mocker.patch.object(requests, "delete", autospec=True, return_value=res_mock)
    res = config.delete_broker_queue("s3i:abcd")
    assert res == res_mock


def test_create_broker_event_queue(mocker: MockerFixture, config: Config):
    res_mock = mocker.Mock(wraps=requests.Response)
    res_mock.status_code = 201
    topic = "TestTopic"
    res_json = {"topic": topic, "queue_length": 0, "ttl": 1}
    res_mock.json.return_value = res_json
    mocker.patch.object(requests, "post", autospec=True, return_value=res_mock)
    res = config.create_broker_event_queue("s3i:abcd", topic)
    assert res == res_mock


def test_create_broker_event_queue__length(mocker: MockerFixture, config: Config):
    res_mock = mocker.Mock(wraps=requests.Response)
    res_mock.status_code = 201
    topic = "TestTopic"
    queue_length = 2
    res_json = {"topic": topic, "queue_length": queue_length, "ttl": 1}
    res_mock.json.return_value = res_json
    mocker.patch.object(requests, "post", autospec=True, return_value=res_mock)
    res = config.create_broker_event_queue("s3i:abcd", topic, queue_length)
    assert res == res_mock


def test_delete_broker_event_queue(mocker: MockerFixture, config: Config):
    res_mock = mocker.Mock(wraps=requests.Response)
    res_mock.status_code = 204
    mocker.patch.object(requests, "delete", autospec=True, return_value=res_mock)
    res = config.delete_broker_event_queue("s3i:abcd")
    assert res == res_mock
