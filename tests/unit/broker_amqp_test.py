import pytest
from pytest_mock import MockerFixture

from s3i.broker import BrokerAMQP, requests
from s3i.config import requests as requests_config
from s3i.exception import S3IError


@pytest.fixture
def token():
    return "abcd"


@pytest.fixture
def endpoint():
    return "endpoint"


# @pytest.fixture
# def callback(mocker: MockFixture):
#     pass
def callback():
    pass


@pytest.fixture
def broker(token: str, endpoint: str):
    return BrokerAMQP(token, endpoint, callback, listenToEvents=False)


@pytest.fixture()
def event_broker(token: str, endpoint: str):
    return BrokerAMQP(token, endpoint, callback, listenToEvents=True)


def test_constuctor(broker: BrokerAMQP, token: str):
    assert broker.token == token
    assert broker.channel == None
    assert broker.connection == None


def test_get_token(broker: BrokerAMQP, token: str):
    assert broker.token == token


def test_set_token(broker: BrokerAMQP):
    new_token = "new_token"
    broker.token = new_token
    assert broker.token == new_token


def test_connect(broker: BrokerAMQP):
    # Connect fails with invalid credentials
    broker.connect()
    assert broker.connection != None


def test_connect_event_broker(event_broker: BrokerAMQP):
    with pytest.raises(S3IError):
        event_broker.connect()
        # TODO: Assertion is not executed, check why.
        assert event_broker.connection == None


def test_on_connection_open(broker: BrokerAMQP):
    # broker.connect()
    pass


def test_add_callback(broker: BrokerAMQP):
    # TODO: Find a way to assert the internal state
    broker.add_on_channel_open_callback(callback, one_shot=True)
    broker.add_on_connection_open_callback(callback, one_shot=True)


def test_start_consuming(broker: BrokerAMQP):
    # broker.start_consuming()
    pass


def test_reconnect_token_expired(broker:BrokerAMQP):
    # broker.reconnect_token_expired("my_token")
    pass

def test_create_event_queue(mocker:MockerFixture, broker: BrokerAMQP):
    res_mock = mocker.Mock(wraps=requests_config.Response)
    res_mock.status_code = 201 
    event_endpoint = "moisture"
    res_mock.json.return_value = {"queue_name": event_endpoint}
    mocker.patch.object(requests_config, "post", autospec=True, return_value=res_mock)
    res = broker.create_event_queue()
    assert res == event_endpoint