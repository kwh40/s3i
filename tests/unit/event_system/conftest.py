import pytest

from s3i.event_system import NamedEvent


@pytest.fixture
def rql_expression_str():
    return "thingid=s3i:abcd"


@pytest.fixture
def topic_named_event():
    return "s3imytopic"


@pytest.fixture
def json_schema_named_event():
    schema = {"type": "object"}
    return schema


@pytest.fixture
def named_event(topic_named_event, json_schema_named_event):
    return NamedEvent(topic_named_event, json_schema_named_event)
