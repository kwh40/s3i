import pytest

from s3i.event_system import EventManager


@pytest.fixture
def entry():
    return {"event_name": "tank_full"}


@pytest.fixture
def manager(entry):
    return EventManager(entry)


def test_constructor(manager: EventManager, entry):
    assert manager.json_entry == entry
    assert manager.named_event_dict == {}
    assert manager.custom_event_dict == {}


def test_json_dict_setter(manager: EventManager):
    new_value = {}
    manager.json_entry = new_value
    assert manager.json_entry == new_value


def test_add_named_event(
    manager: EventManager, topic_named_event, json_schema_named_event
):
    assert manager.add_named_event(topic_named_event, json_schema_named_event) == True
    assert manager.add_named_event(topic_named_event, json_schema_named_event) == False


def test_delete_named_event(manager: EventManager, topic_named_event, json_schema_named_event):
    assert manager.delete_named_event(topic_named_event) == False
    assert manager.add_named_event(topic_named_event, json_schema_named_event) == True
    assert manager.delete_named_event(topic_named_event) == None


def test_add_custom_event__invalid_rql(manager: EventManager):
    rql_expression = "test"
    attribute_path = "moisture"
    assert manager.add_custom_event(rql_expression, attribute_path) == (False, None)


def test_add_custom_event__invalid_path(manager: EventManager, rql_expression_str):
    attribute_path = "moisture"
    assert manager.add_custom_event(rql_expression_str, attribute_path) == (False, None)