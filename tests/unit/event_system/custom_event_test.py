import pytest

from s3i.event_system import CustomEvent


@pytest.fixture
def filter_object():
    return "filter"


@pytest.fixture
def filter_attribute_paths():
    return ["thingId", "name"]


@pytest.fixture
def attribute_paths():
    return ["thingId", "name"]


@pytest.fixture
def topic():
    return "mytopic"


@pytest.fixture
def json_entry():
    return {"thingId": "s3i:abcd"}


@pytest.fixture
def event(filter_object, filter_attribute_paths, attribute_paths, topic, json_entry):
    return CustomEvent(
        filter_object, filter_attribute_paths, attribute_paths, topic, json_entry
    )


def test_constructor(
    event: CustomEvent,
    filter_object,
    filter_attribute_paths,
    attribute_paths,
    topic
):
    event.filter_object == filter_object
    event.filter_attribute_paths == filter_attribute_paths
    event.attribute_paths == attribute_paths
    event.topic == topic
    # event.msg == {}


# def test_check_filter(event: CustomEvent):
#     assert event.check_filter() == False