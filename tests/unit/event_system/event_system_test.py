import pytest

from s3i.event_system import _getValue, _uriToData, resGetValue


@pytest.fixture
def model():
    return {"features": [{"moisture": 10}]}


def test_uri_to_data__uri_empty(model):
    uri = ""
    assert _uriToData(uri, model) == model


def test_uri_to_data__features(model):
    uri = "features"
    assert _uriToData(uri, model) == model["features"]

    uri = "features/moisture"
    assert _uriToData(uri, model) == None


def test_uri_to_data__values(model):
    uri = "moisture"
    assert _uriToData(uri, model) == None
