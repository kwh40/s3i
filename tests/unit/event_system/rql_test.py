import pytest
from pyrql.query import Filter

from s3i.event_system import RQL


@pytest.fixture
def expression(rql_expression_str):
    return RQL(rql_expression_str)


def test_constructor(expression: RQL, rql_expression_str):
    expression = RQL(rql_expression_str)
    assert expression.rql_expression == rql_expression_str
    assert expression.parsed_rql_expression == {
        "args": ["thingid", "s3i:abcd"],
        "name": "eq",
    }
    assert expression.is_rql_valid == True


def test_constructor__invalid_eql_expression():
    rql_expression = "test"
    expression = RQL(rql_expression)
    assert expression.rql_expression == rql_expression
    assert expression.parsed_rql_expression == {}
    assert expression.is_rql_valid == False


# def test_make_filter(expression: RQL):
#     assert expression.make_filter() == 