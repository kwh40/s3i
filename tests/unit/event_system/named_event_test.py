from s3i.event_system import NamedEvent


def test_construct(named_event: NamedEvent, topic_named_event, json_schema_named_event):
    assert named_event.topic == topic_named_event
    assert named_event.json_schema == json_schema_named_event
    assert named_event.msg == {}


def test_generate_event_msg(named_event: NamedEvent, topic_named_event):
    content =  {"mycontent": "content"}
    named_event.generate_event_msg(content)
    msg = named_event.msg
    assert msg["sender"] == topic_named_event
    assert msg["identifier"].startswith("s3i")
    assert msg["topic"] == topic_named_event
    assert msg["content"] == content
