
import json

import pgpy
import pytest

from s3i.broker_message import SetValueReply


def test_constructor():
    msg = SetValueReply()
    assert msg.base_msg == {}
    assert msg.pgp_msg.message == pgpy.PGPMessage.new("").message
    assert msg.gzip_msg == b""


def test_constructor__base_msg_variations(base_msg):
    if isinstance(base_msg, dict):
        msg = SetValueReply(base_msg=base_msg)
        assert msg.base_msg == base_msg
        assert msg.pgp_msg.message == pgpy.PGPMessage.new("").message
        assert msg.gzip_msg == b""
    else:
        with pytest.raises(json.decoder.JSONDecodeError):
            SetValueReply(base_msg=base_msg)

    
def test_fill_message():
    message = SetValueReply()
    sender = "s3i:abcd"
    receivers = ["s3i:efgh"]
    message_id = "s3i:1"
    ok = True
    reply_to_endpoint = "s3ib:efgh"
    reply_to_message = "s3i:2"
    message.fillSetValueReply(
        sender,
        receivers,
        message_id,
        ok,
        reply_to_endpoint,
        reply_to_message,
    )
    msg = message.base_msg
    assert msg["sender"] == sender
    assert msg["identifier"] == message_id
    assert msg["receivers"] == receivers
    assert msg["messageType"] == "setValueReply"
    assert msg["replyToEndpoint"] == reply_to_endpoint
    assert msg["ok"] == ok
