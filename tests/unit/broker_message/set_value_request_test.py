import json

import pgpy
import pytest

from s3i.broker_message import SetValueRequest


def test_constructor():
    msg = SetValueRequest()
    assert msg.base_msg == {}
    assert msg.pgp_msg.message == pgpy.PGPMessage.new("").message
    assert msg.gzip_msg == b""


def test_constructor__base_msg_variations(base_msg):
    if isinstance(base_msg, dict):
        msg = SetValueRequest(base_msg=base_msg)
        assert msg.base_msg == base_msg
        assert msg.pgp_msg.message == pgpy.PGPMessage.new("").message
        assert msg.gzip_msg == b""
    else:
        with pytest.raises(json.decoder.JSONDecodeError):
            SetValueRequest(base_msg=base_msg)

    
def test_fill_message():
    message = SetValueRequest()
    sender = "s3i:abcd"
    receivers = ["s3i:efgh"]
    message_id = "s3i:1"
    attribute_path = "my_attribute"
    new_value = 12
    reply_to_endpoint = "s3ib:efgh"
    reply_to_message = "s3i:2"
    message.fillSetValueRequest(
        sender,
        receivers,
        message_id,
        attribute_path,
        new_value,
        reply_to_endpoint,
        reply_to_message,
    )
    msg = message.base_msg
    assert msg["sender"] == sender
    assert msg["identifier"] == message_id
    assert msg["receivers"] == receivers
    assert msg["messageType"] == "setValueRequest"
    assert msg["replyToEndpoint"] == reply_to_endpoint
    assert msg["attributePath"] == attribute_path
    assert msg["newValue"] == new_value
