import gzip
import json

import pgpy
import pytest
from pgpy.constants import KeyFlags

from s3i.broker_message import UserMessage


@pytest.fixture
def user_message():
    return UserMessage()


def test_constructor__pgp_msg():
    msg_str = "Hello world!"
    message = pgpy.PGPMessage.new(msg_str)
    msg = UserMessage(pgp_msg=message)
    assert msg.pgp_msg.message == msg_str


def test_constructor__pgp_msg__blob():
    msg_str = "Hello world!"
    # Transform blob into OpenPGP-compliant ASCII-armored format.
    blob = str(pgpy.PGPMessage.new(msg_str))
    msg = UserMessage(pgp_msg=blob)
    assert msg.pgp_msg.message == msg_str


def test_constructor__gzip(gzip_message):
    msg = UserMessage(gzip_msg=gzip_message)
    assert msg.gzip_msg == gzip_message


def test_constructor__gzip__throw():
    gzip_msg = b"Hello world!"
    with pytest.raises(ValueError):
        UserMessage(gzip_msg=gzip_msg)


def test_base_msg_setter__throw(user_message: UserMessage):
    with pytest.raises(TypeError):
        user_message.base_msg = "Greetings"


def test_pgp_msg_setter(user_message: UserMessage):
    pgp_msg = pgpy.PGPMessage.new("Hello world!")
    user_message.pgp_msg = pgp_msg
    assert user_message.pgp_msg == pgp_msg


def test_pgp_msg_setter__throw(user_message: UserMessage):
    with pytest.raises(TypeError):
        user_message.pgp_msg = "my new message"


def test_gzip_msg_setter(user_message: UserMessage, gzip_message):
    user_message.gzip_msg = gzip_message
    assert user_message.gzip_msg == gzip_message


def test_gzip_msg_setter__throw_value_error(user_message: UserMessage):
    with pytest.raises(ValueError):
        user_message.gzip_msg = b"Hello world!"


def test_gzip_msg_setter__throw_type_error(user_message: UserMessage):
    with pytest.raises(TypeError):
        user_message.gzip_msg = "Hello world!"


def test_convert_base_to_pgp(user_message: UserMessage, base_msg_dict):
    user_message.base_msg = base_msg_dict
    user_message.convert_base_to_pgp()
    assert user_message.pgp_msg.message == json.dumps(base_msg_dict)


def test_convert_pgp_to_base(user_message: UserMessage, pgp_msg, base_msg_dict):
    user_message.pgp_msg = pgp_msg
    user_message.convert_pgp_to_base()
    assert user_message.base_msg == base_msg_dict


def test_convert_pgp_to_base__encrypted(user_message: UserMessage, pgp_msg):
    pgp_msg_enc = pgp_msg.encrypt("pwd")
    user_message.pgp_msg = pgp_msg_enc
    with pytest.raises(ValueError):
        user_message.convert_pgp_to_base()


def test_convert_gzip_to_pgp(user_message: UserMessage):
    res = user_message.convert_gzip_to_pgp()
    assert res.message == ""


def test_convert_pgp_to_gzip(user_message: UserMessage, gzip_message):
    pgp_msg = pgpy.PGPMessage.new(gzip_message)
    user_message.pgp_msg = pgp_msg
    msg = user_message.convert_pgp_to_gzip()
    assert msg == gzip_message


def test_convert_pgp_to_gzip__throw_encrypted(user_message: UserMessage, pgp_msg):
    msg_enc = pgp_msg.encrypt("asdf")
    user_message.pgp_msg = msg_enc
    with pytest.raises(ValueError) as error:
        user_message.convert_pgp_to_gzip()
        assert str(error).contsins("encrypted")


def test_convert_pgp_to_gzip__throw(user_message: UserMessage, pgp_msg):
    user_message.pgp_msg = pgp_msg
    with pytest.raises(ValueError):
        user_message.convert_pgp_to_gzip()


def test_sign__throw(user_message: UserMessage):
    with pytest.raises(TypeError):
        user_message.sign("my_key", "my_message")
    key = pgpy.PGPKey()
    with pytest.raises(TypeError):
        user_message.sign(key, "my_message")


def test_sign(user_message, pgp_msg, pgp_key):
    assert pgp_msg.is_signed == False
    res = user_message.sign(pgp_key, pgp_msg)
    assert res.is_signed


def test_verify(user_message: UserMessage, pgp_msg_signed, pgp_key):
    # Return = True
    assert user_message.verify(pgp_key.pubkey, pgp_msg_signed)

    # Return = False
    uid = pgpy.PGPUID.new("Max Maier")
    key = pgpy.PGPKey.new(pgpy.constants.PubKeyAlgorithm.RSAEncryptOrSign, 512)
    key.add_uid(uid, usage=KeyFlags.Sign)
    assert user_message.verify(key.pubkey, pgp_msg_signed) == False


def test_verify__throw_type_error(user_message: UserMessage):
    with pytest.raises(TypeError):
        user_message.verify("my_key", "my_message")
    key = pgpy.PGPKey()
    with pytest.raises(TypeError):
        user_message.verify(key, "my_message")


def test_encrypt_decrypt(user_message: UserMessage, pgp_key, pgp_msg):
    msg_enc = user_message.encrypt(pgp_key.pubkey, pgp_msg)
    assert msg_enc.is_encrypted == True
    msg = user_message.decrypt(pgp_key, msg_enc)
    assert msg.message == pgp_msg.message


def test_encrypt__throw_type_error(user_message: UserMessage, pgp_key, pgp_msg):
    with pytest.raises(TypeError):
        user_message.encrypt("pubkey", pgp_msg)

    with pytest.raises(TypeError):
        user_message.encrypt(pgp_key, "my_messsage")


def test_decrypt__throw_type_error(user_message: UserMessage, pgp_key, pgp_msg):
    with pytest.raises(TypeError):
        user_message.decrypt("pubkey", pgp_msg)

    with pytest.raises(TypeError):
        user_message.decrypt(pgp_key, "my_messsage")

def test_compress_decompress(user_message: UserMessage, base_msg_dict):
    msg_comp = user_message.compress(base_msg_dict, 2)
    msg = user_message.decompress(msg_comp)
    assert msg == base_msg_dict

def test_compress__throw(user_message: UserMessage, base_msg_dict):
    with pytest.raises(TypeError):
        user_message.compress(base_msg_dict, "12")



def test_decompress__throw(user_message: UserMessage):
    with pytest.raises(TypeError) as error:
        user_message.decompress("my_message")
    # message is of correct type but doesn't start with
    # GZIP_MAGIC_NUMBER
    with pytest.raises(TypeError):
        user_message.decompress(b"my_message")
