import json

import pgpy
import pytest

from s3i.broker_message import ServiceRequest


def test_constructor():
    msg = ServiceRequest()
    assert msg.base_msg == {}
    assert msg.pgp_msg.message == pgpy.PGPMessage.new("").message
    assert msg.gzip_msg == b""


def test_constructor__base_msg_variations(base_msg):
    if isinstance(base_msg, dict):
        msg = ServiceRequest(base_msg=base_msg)
        assert msg.base_msg == base_msg
        assert msg.pgp_msg.message == pgpy.PGPMessage.new("").message
        assert msg.gzip_msg == b""
    else:
        with pytest.raises(json.decoder.JSONDecodeError):
            ServiceRequest(base_msg=base_msg)

    
def test_fill_message():
    message = ServiceRequest()
    sender = "s3i:abcd"
    receivers = ["s3i:efgh"]
    message_id = "s3i:1"
    service_type = "my_service"
    parameters = {"filename": "my_attachements", "data": "0001110"}
    reply_to_endpoint = "s3ib:efgh"
    reply_to_message = "s3i:2"
    message.fillServiceRequest(
        sender,
        receivers,
        message_id,
        service_type,
        parameters,
        reply_to_endpoint,
        reply_to_message,
    )
    msg = message.base_msg
    assert msg["sender"] == sender
    assert msg["identifier"] == message_id
    assert msg["receivers"] == receivers
    assert msg["messageType"] == "serviceRequest"
    assert msg["replyToEndpoint"] == reply_to_endpoint
    assert msg["parameters"] == parameters

