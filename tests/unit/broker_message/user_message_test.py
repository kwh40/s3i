import json

import pgpy
import pytest

from s3i.broker_message import UserMessage


def test_constructor():
    message = UserMessage()
    assert message.base_msg == {}
    assert message.pgp_msg.message == pgpy.PGPMessage.new("").message
    assert message.gzip_msg == b""


def test_constructor__base_msg_variations(base_msg):
    # NOTE: Differentiation between dict and other types should
    # not be necessary. Behavior is just broken right now.
    # See: https://git.rwth-aachen.de/kwh40/s3i/-/issues/34
    if isinstance(base_msg, dict):
        message = UserMessage(base_msg)
        assert message.base_msg == base_msg
        assert message.pgp_msg.message == pgpy.PGPMessage.new("").message
        assert message.gzip_msg == b""
    else:
        with pytest.raises(json.decoder.JSONDecodeError):
            message = UserMessage(base_msg=base_msg)


def test_fill_message():
    message = UserMessage()
    sender = "s3i:abcd"
    receivers = ["s3i:efgh"]
    subject = "Greetings"
    text = "Hello world!"
    message_id = "s3i:1"
    reply_to_endpoint = "s3ib:efgh"
    reply_to_message = "s3i:2"
    attachments = [{"filename": "my_attachements", "data": "0001110"}]
    message.fillUserMessage(
        sender,
        receivers,
        subject,
        text,
        message_id,
        reply_to_endpoint,
        reply_to_message,
        attachments,
    )
    msg = message.base_msg
    assert msg["sender"] == sender
    assert msg["receivers"] == receivers
    assert msg["subject"] == subject
    assert msg["text"] == text
    assert msg["identifier"] == message_id
    assert msg["replyToEndpoint"] == reply_to_endpoint
    assert msg["attachments"][0] == attachments[0]
