import json

import pgpy
import pytest

from s3i.broker_message import GZIP_MAGIC_NUMBER


@pytest.fixture(
    params=[
        "Hello world!",
        b"Hello world!",
        {
            "sender": "s3i:abcd",
            "identifier": "s3i:1",
            "receivers": ["s3i:efgh"],
            "messageType": "userMessage",
            "subject": "Greetings",
            "text": "Hello world!",
        },
    ]
)
def base_msg(request):
    return request.param


@pytest.fixture
def base_msg_dict():
    tmp = {
        "sender": "s3i:abcd",
        "identifier": "s3i:1",
        "receivers": ["s3i:efgh"],
        "messageType": "userMessage",
        "subject": "Greetings",
        "text": "Hello world!",
    }
    return tmp


@pytest.fixture
def pgp_msg(base_msg_dict):
    return pgpy.PGPMessage.new(json.dumps(base_msg_dict))


@pytest.fixture
def pgp_msg_signed(pgp_msg, pgp_key):
    pgp_msg |= pgp_key.sign(pgp_msg)
    return pgp_msg


@pytest.fixture
def gzip_message():
    tmp = bytes(bytearray.fromhex(GZIP_MAGIC_NUMBER))
    gzip_msg = tmp + b"Hello world!"
    return gzip_msg
