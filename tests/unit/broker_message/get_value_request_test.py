import json

import pgpy
import pytest

from s3i.broker_message import GetValueRequest, TimeInterval


def test_constructor():
    message = GetValueRequest()
    assert message.base_msg == {}
    assert message.pgp_msg.message == pgpy.PGPMessage.new("").message
    assert message.gzip_msg == b""


def test_constructor__base_msg_variations(base_msg):
    # NOTE: Differentiation between dict and other types should
    # not be necessary. Behavior is just broken right now.
    # See: https://git.rwth-aachen.de/kwh40/s3i/-/issues/34
    if isinstance(base_msg, dict):
        message = GetValueRequest(base_msg)
        assert message.base_msg == base_msg
        assert message.pgp_msg.message == pgpy.PGPMessage.new("").message
        assert message.gzip_msg == b""
    else:
        with pytest.raises(json.decoder.JSONDecodeError):
            message = GetValueRequest(base_msg=base_msg)


def test_fill_messag_timeinterval():
    msg = GetValueRequest()
    sender = "s3i:abcd"
    receivers = ["s3i:efgh"]
    message_id = "s3i:1"
    attribute_path = "my_service"
    reply_to_endpoint = "s3ib:efgh"
    reply_to_message = "s3i:2"
    time_start = 1675788330
    time_end = 1675874730
    msg.fillGetValueRequest(
        sender,
        receivers,
        message_id,
        attribute_path,
        reply_to_endpoint,
        reply_to_message,
        time_interval=TimeInterval(start=time_start, end=time_end)
    )
    msg = msg.base_msg
    assert msg["sender"] == sender
    assert msg["identifier"] == message_id
    assert msg["receivers"] == receivers
    assert msg["messageType"] == "getValueRequest"
    assert msg["replyToEndpoint"] == reply_to_endpoint
    assert msg["attributePath"] == attribute_path
    assert msg["timeInterval"]["start"] == time_start
    assert msg["timeInterval"]["end"] == time_end

def test_fill_messag_timeinterval_dict():
    msg = GetValueRequest()
    sender = "s3i:abcd"
    receivers = ["s3i:efgh"]
    message_id = "s3i:1"
    attribute_path = "my_service"
    reply_to_endpoint = "s3ib:efgh"
    reply_to_message = "s3i:2"
    time_start = 1675788330
    time_end = 1675874730
    msg.fillGetValueRequest(
        sender,
        receivers,
        message_id,
        attribute_path,
        reply_to_endpoint,
        reply_to_message,
        time_interval={"start": time_start, "end": time_end}
    )
    msg = msg.base_msg
    assert msg["sender"] == sender
    assert msg["identifier"] == message_id
    assert msg["receivers"] == receivers
    assert msg["messageType"] == "getValueRequest"
    assert msg["replyToEndpoint"] == reply_to_endpoint
    assert msg["attributePath"] == attribute_path
    assert msg["timeInterval"]["start"] == time_start
    assert msg["timeInterval"]["end"] == time_end