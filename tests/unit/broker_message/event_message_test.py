import json
from datetime import datetime

import pgpy
import pytest

from s3i.broker_message import EventMessage


def test_constructor():
    message = EventMessage()
    assert message.base_msg == {}
    assert message.pgp_msg.message == pgpy.PGPMessage.new("").message
    assert message.gzip_msg == b""


def test_constructor__base_msg_variations(base_msg):
    # NOTE: Differentiation between dict and other types should
    # not be necessary. Behavior is just broken right now.
    # See: https://git.rwth-aachen.de/kwh40/s3i/-/issues/34
    if isinstance(base_msg, dict):
        message = EventMessage(base_msg)
        assert message.base_msg == base_msg
        assert message.pgp_msg.message == pgpy.PGPMessage.new("").message
        assert message.gzip_msg == b""
    else:
        with pytest.raises(json.decoder.JSONDecodeError):
            message = EventMessage(base_msg=base_msg)


def test_fill_messag():
    msg = EventMessage()
    sender = "s3i:abcd"
    message_id = "s3i:1"
    topic = "my_topic"
    timestamp = int(datetime.now().timestamp())
    content = { "content": "my_content" }
    reply_to_endpoint = "s3ib:efgh"
    reply_to_message = "s3i:2"
    msg.fillEventMessage(
        sender,
        message_id,
        topic,
        timestamp,
        content,
        reply_to_endpoint,
        reply_to_message,
    )
    msg = msg.base_msg
    assert msg["sender"] == sender
    assert msg["identifier"] == message_id
    assert msg["messageType"] == "eventMessage"
    assert msg["replyToEndpoint"] == reply_to_endpoint
    assert msg["topic"] == topic
    assert msg["timestamp"] == timestamp
    assert msg["content"] == content
