import pytest
from pytest_mock import MockerFixture

from s3i.broker import BrokerREST, requests
from s3i.exception import S3IBrokerRESTError

URL_BROKER = "https://broker.s3i.vswf.dev/"
TOKEN = "abcd"


@pytest.fixture
def broker():
    return BrokerREST(TOKEN)


@pytest.fixture(params=[False, True])
def encrypted(request):
    yield request.param


def test_constructor(broker: BrokerREST):
    assert broker._token == TOKEN
    assert broker._url == URL_BROKER
    # Assert headers
    c_type = "Content-Type"
    assert c_type in broker.headers
    assert broker.headers[c_type] == "application/json"
    auth = "Authorization"
    assert auth in broker.headers
    bearer_token = f"Bearer {TOKEN}"
    assert broker.headers[auth] == bearer_token
    # Assert encrypted headers
    assert c_type in broker.headers_encrypted
    assert broker.headers_encrypted[c_type] == "application/pgp-encrypted"
    assert auth in broker.headers_encrypted
    assert broker.headers_encrypted[auth] == bearer_token


def test_token(broker: BrokerREST):
    assert broker.token == TOKEN


def test_set_token(broker: BrokerREST):
    new_token = "new"
    broker.token = new_token
    assert broker.token == new_token


def test_send(mocker: MockerFixture, broker: BrokerREST, encrypted):
    # Define function parameters
    msg = "Hello world!"

    # Patch requests.post
    response_mock = mocker.Mock(wraps=requests.Response)
    response_mock.status_code = 201
    response_json = {"message": msg}
    response_mock.json.return_value = response_json
    mocker.patch.object(
        requests, "post", autospec=True, return_value=response_mock
    )

    # Test with endpoints == None
    endpoints = None
    with pytest.raises(TypeError):
        broker.send(endpoints, msg, encrypted)

    endpoints = 2
    with pytest.raises(TypeError):
        broker.send(endpoints, msg, encrypted)

    # Test with type(endpoints) != list
    endpoints = "string"
    response = broker.send(endpoints, msg, encrypted)
    assert response == True

    endpoints = ["dz"]
    response = broker.send(endpoints, msg, encrypted)
    assert response == True

def test_receive_once(mocker: MockerFixture, broker: BrokerREST):
    response_mock = mocker.Mock(wraps=requests.Response)
    response_mock.status_code = 200
    response_json = {"message": "Hello world!"}
    response_mock.json.return_value = response_json
    mocker.patch.object(requests, "get", autospec=True, return_value=response_mock)

    response = broker.receive_once("this_is_a_queue") 
    assert response == response_json