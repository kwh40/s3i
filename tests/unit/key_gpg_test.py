from pathlib import Path

import pgpy
import pytest
from pytest_mock import MockerFixture

from s3i.key_pgp import Key


def test_constructor():
    # Default constructor
    key = Key()
    assert type(key.key) == pgpy.PGPKey

    # Construct instance from file
    path = Path("./") / "tests" / "unit" / "res"
    filename = "test_key.asc"
    key = Key(str(path.absolute()), filename)
    assert type(key.key) == pgpy.PGPKey

    # Construct instance from blob
    path = path / filename
    with path.open("r") as handler:
        key_blob = handler.read()
    key = Key(key_str=key_blob)
    assert type(key.key) == pgpy.PGPKey


def test_generateKey():
    name = "kwh40"
    comment = "kwh40 private key"
    email = "kwh40@kwh.de"

    # NOTE: comparing str version of public keys
    # fails because it changes by adding meta
    # information.
    key = Key()

    # NOTE: encrypting a message with a key, that has
    # no uid fails.
    pgp_key = key.key
    message_text = "My awesome message."
    pgp_message = pgpy.PGPMessage.new(message_text)
    with pytest.raises(pgpy.errors.PGPError):
        pgp_key.encrypt(pgp_message)

    with pytest.raises(pgpy.errors.PGPError):
        pgp_key.pubkey.encrypt(pgp_message)

    key.generateKey(name, comment, email)
    assert key.key.is_public == False
    uids = key.key.userids
    assert len(uids) == 1
    uid = uids[0]
    assert uid.name == name
    assert uid.comment == comment
    assert uid.email == email

    # Message encryption with a key that has a uid attached works.
    encrypted_message = key.key.pubkey.encrypt(pgp_message)
    assert key.key.decrypt(encrypted_message).message == message_text


def test_addKeyExpiration():
    key = Key()
    with pytest.raises(AttributeError):
        key.addKeyExpiration(12)


@pytest.fixture(params=[None, "tmp"])
def filepath(request):
    return request.param


def test_exportsecKeyToFile(mocker: MockerFixture, filepath):
    key = Key()
    mock = mocker.patch("builtins.open", mocker.mock_open(), create=True)
    key.exportsecKeyToFile(path=filepath)
    mock.assert_called()
    # TODO: Assert file content


def test_exportPubKeyToFile(mocker: MockerFixture, filepath):
    key = Key()
    mock = mocker.patch("builtins.open", mocker.mock_open(), create=True)
    key.exportPubKeyToFile(path=filepath)
    mock.assert_called()
    # TODO: Assert file content
