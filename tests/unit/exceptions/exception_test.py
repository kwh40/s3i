import json

import pytest
import requests
from pika.exceptions import ChannelError
from pytest_mock import MockerFixture

from s3i.exception import (S3IError, raise_error_from_response,
                           raise_error_from_s3ib_amqp,
                           raise_error_from_s3ib_msg)


@pytest.fixture(
    params=[
        "userMessage",
        "serviceReply",
        "serviceRequest",
        "getValueRequest",
        "getValueReply",
        "setValueRequest",
        "setValueReply",
        "createAttributeRequest",
        "createAttributeReply",
        "deleteAttributeRequest",
        "deleteAttributeReply",
        "subscribeCustomEventRequest",
        "subscribeCustomEventReply",
        "unsubscribeCustomEventRequest",
        "unsubscribeCustomEventReply",
        "eventMessage",
        "MyInventedUnknownMessageType",
    ]
)
def message_type(request):
    return request.param


def test_raise_error_from_s3ib_amqp():
    def raise_channel_error():
        raise ChannelError()

    with pytest.raises(S3IError) as error:
        raise_error_from_s3ib_amqp(raise_channel_error, S3IError)
        assert type(error) == S3IError


def test_raise_error_from_s3ib_amqp__no_throw():
    def calc():
        return 10

    res = raise_error_from_s3ib_amqp(calc, S3IError)
    assert res == calc()


def test_raise_error_from_s3ib_msg__throw():
    msg = "error"
    with pytest.raises(S3IError):
        raise_error_from_s3ib_msg(msg, S3IError)


def test_raise_error_from_s3ib_msg(message_type):
    msg = {"messageType": message_type}
    with pytest.raises(S3IError):
        raise_error_from_s3ib_msg(msg, S3IError)


def test_raise_error_from_response(mocker: MockerFixture):
    def raise_json_error():
        raise json.decoder.JSONDecodeError("msg", "doc", 10)

    status_code = 200
    res_mock = mocker.Mock(requests.Response)
    res_mock.status_code = status_code
    res_mock.json = raise_json_error
    assert raise_error_from_response(res_mock, S3IError, status_code) == {}


def test_raise_error_from_response__throw(mocker: MockerFixture):
    def raise_json_error():
        raise json.decoder.JSONDecodeError("msg", "doc", 10)

    res_mock = mocker.Mock(requests.Response)
    res_mock.status_code = 200
    res_mock.json = raise_json_error
    res_mock.text = "Erorr"
    with pytest.raises(S3IError):
        raise_error_from_response(res_mock, S3IError, 201)
