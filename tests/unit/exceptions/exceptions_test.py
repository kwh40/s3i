import pytest

from s3i.exception import (S3IBMessageError, S3IBrokerAMQPError,
                           S3IBrokerRESTError, S3IDirectoryError,
                           S3IDittoError, S3IError, S3IIdentityProviderError)


def test_S3IError():
    msg = "error"
    err = S3IError(msg)
    assert err.__str__() == msg


def test_S3IDittoError():
    msg_str = "error"
    err = S3IDittoError(msg_str)
    assert err.__str__() == msg_str

    msg = {"message": msg_str}
    err = S3IDittoError(msg)
    assert err.__str__() == msg_str
    assert str(err) == msg_str


def test_S3IDirectoryError():
    msg_str = "error"
    err = S3IDirectoryError(msg_str)
    assert err.__str__() == None
    # NOTE: 100% coverage is not possible, because the
    # defined code can't be executed.
    # assert err.__str__() == msg_str


def test_S3IIdentityProviderError():
    msg_str = "error"
    err = S3IIdentityProviderError(msg_str)
    assert err.__str__() == msg_str

    msg = {"error": msg_str}
    err = S3IIdentityProviderError(msg)
    assert err.__str__() == msg_str
    assert str(err) == msg_str


def test_S3IBMessageError():
    msg_str = "error"
    err = S3IBMessageError(msg_str)
    assert err.__str__() == msg_str


def test_S3IBrokerRESTError():
    msg_str = "error"
    err = S3IBrokerRESTError(msg_str)
    # This return value is wierd.
    with pytest.raises(TypeError):
        str(err)

    msg_dict = {"error": msg_str}
    err = S3IBrokerRESTError(msg_dict)
    assert str(err) == msg_str

    msg = [msg_dict]
    err = S3IBrokerRESTError(msg)
    assert str(err) == msg_str


def test_S3IBrokerAMQPError():
    msg_str = "error"
    err = S3IBrokerAMQPError(msg_str)
    assert str(err) == msg_str

    err = S3IBrokerAMQPError(None)
    assert str(err) == "unknown error"

