import pytest
from pytest_mock import MockerFixture

from s3i.ditto_manager import DittoManager, requests
from s3i.tools import requests as tools_req


@pytest.fixture
def token():
    return "abcd"


@pytest.fixture
def manager(directory_url, token):
    return DittoManager(directory_url, token)


def test_set_url(manager: DittoManager):
    new_url = "new_url"
    manager.ditto_url = new_url
    assert manager.ditto_url == new_url


def test_set_token(manager: DittoManager):
    new_token = "new_token"
    manager.token = new_token
    assert manager.token == new_token


def test_set_headers(manager: DittoManager):
    new_heaeders = {"Content-Type": "xml", "Authorization": "Bearer abcd"}
    manager.ditto_headers = new_heaeders
    assert manager.ditto_headers == new_heaeders


def test_pass_refreshed_token(manager: DittoManager):
    new_token = "new_token"
    manager.pass_refreshed_token(new_token)
    new_headers = {
        "Content-Type": "application/json",
        "Authorization": f"Bearer {new_token}",
    }
    assert manager.token == new_token
    assert manager.ditto_headers == new_headers


def test_updateThingIDBased(mocker: MockerFixture, manager: DittoManager):
    res_mock = mocker.Mock(wrap=requests.Response)
    res_mock.status_code = 204
    res_json = {"response": "success"}
    res_mock.json.return_value = res_json
    mocker.patch.object(requests, "put", autospec=True, return_value=res_mock)

    res = manager.updateThingIDBased("s3i:abcd", {"payload": "123"})
    assert res == res_mock


def test_queryThingIDBased(mocker: MockerFixture, manager: DittoManager):
    res_mock = mocker.Mock(wrapper=requests.Response)
    res_mock.status_code = 200
    thing_id = "s3i:abcd"
    res_json = {"thing_id": thing_id}
    res_mock.json.return_value = res_json

    mocker.patch.object(requests, "get", autospec=True, return_value=res_mock)
    res = manager.queryThingIDBased(thing_id)
    assert res == res_json


def test_queryAttributeBased(mocker: MockerFixture, manager: DittoManager):
    res_mock = mocker.Mock(wrap=tools_req.Response)
    res_mock.status_code = 200
    res_json = [{"thing_id": "s3i:abcd"}]
    res_mock.json.return_value = {"items": res_json}
    mocker.patch.object(tools_req, "get", autospec=True, return_value=res_mock)
    res = manager.queryAttributeBased("key", "value")
    assert res == res_json


def test_searchAll(mocker: MockerFixture, manager: DittoManager):
    res_mock = mocker.Mock(wrap=tools_req.Response)
    res_mock.status_code = 200
    res_json = [{"thing_id": "s3i:abcd"}]
    res_mock.json.return_value = {"items": res_json}
    mocker.patch.object(tools_req, "get", autospec=True, return_value=res_mock)
    res = manager.searchAll("thing_id")
    assert res == res_json


def test_updatePolicyIDBased(mocker: MockerFixture, manager: DittoManager):
    res_mock = mocker.Mock(wrapper=requests.Response)
    res_mock.status_code = 204
    res_json = {"response": "success"}
    res_mock.json.return_value = res_json
    mocker.patch.object(requests, "put", autospec=True, return_value=res_mock)

    policy_id = "s3i:abcd"
    payload = {"policy_id": policy_id}
    res = manager.updatePolicyIDBased(policy_id, payload)
    assert res == res_mock


def test_queryPolicyIDBased(mocker: MockerFixture, manager: DittoManager):
    res_mock = mocker.Mock(wrapper=requests.Response)
    res_mock.status_code = 200
    policy_id = "s3i:abcd"
    res_json = {"policy_id": policy_id}
    res_mock.json.return_value = res_json

    mocker.patch.object(requests, "get", autospec=True, return_value=res_mock)
    res = manager.queryPolicyIDBased(policy_id)
    assert res == res_json
