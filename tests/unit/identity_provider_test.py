from urllib.parse import urljoin

import pytest
from pytest_mock import MockerFixture

from s3i.identity_provider import (IdentityProvider, TokenType, jwt, requests,
                                   threading, webbrowser)


@pytest.fixture
def grant_type():
    return "password"


@pytest.fixture
def client_id():
    return "admin-cli"


@pytest.fixture
def client_secret():
    return "pssst"


@pytest.fixture
def realm():
    return "MK"


@pytest.fixture
def idp_url():
    return "https://idp.s3i.vswf.dev/"


@pytest.fixture
def username():
    return "root"


@pytest.fixture
def password():
    return "toor"


@pytest.fixture
def identity_provider(
    grant_type, client_id, client_secret, realm, idp_url, username, password
):
    return IdentityProvider(
        grant_type, client_id, client_secret, realm, idp_url, username, password
    )


def test_constructor(
    identity_provider: IdentityProvider,
    grant_type,
    client_id,
    client_secret,
    realm,
    idp_url,
    username,
    password,
):
    assert identity_provider._grant_type == grant_type
    assert identity_provider.grand_type == grant_type
    assert identity_provider._client_id == client_id
    assert identity_provider.client_id == client_id
    assert identity_provider._client_secret == client_secret
    assert identity_provider.client_secret == client_secret
    assert identity_provider._realm == realm
    assert identity_provider.realm == realm
    assert identity_provider._username == username
    assert identity_provider.username == username
    assert identity_provider._password == password
    assert identity_provider.password == password
    assert identity_provider._identity_provider_url == idp_url
    assert identity_provider.identity_provider_url == idp_url
    assert identity_provider._token_bundle == None
    assert identity_provider._token_inspector_run == False
    assert identity_provider._last_login == 0
    base_url = f"{idp_url}/auth/realms/{realm}/protocol/openid-connect/"
    token_url = urljoin(base_url, "token")
    assert identity_provider.identity_provider_get_token == token_url
    header = {"Content-Type": "application/x-www-form-urlencoded"}
    assert identity_provider.identity_provider_header == header


def test_stop_run_forever(identity_provider: IdentityProvider):
    identity_provider._token_inspector_run = True
    assert identity_provider._token_inspector_run == True
    identity_provider.stop_run_forever()
    assert identity_provider._token_inspector_run == False


def test_get_token__token_inspector_run_false(identity_provider: IdentityProvider):
    identity_provider._token_inspector_run = True
    assert identity_provider.get_token(TokenType.ACCESS_TOKEN) == None


@pytest.fixture(params=[-200, 200])
def expires_in(request):
    return request.param


@pytest.fixture
def token_bundle(expires_in):
    return {
        "access_token": "my_access_token",
        "id_token": "my_id_token",
        "refresh_token": "my_refresh_token",
        "expires_in": expires_in,
    }


@pytest.fixture
def identity_provider_patched_post(
    mocker: MockerFixture, identity_provider: IdentityProvider, token_bundle
):
    res_mock = mocker.Mock(wrap=requests.Response)
    res_mock.status_code = 200
    res_json = token_bundle
    res_mock.json.return_value = res_json
    mocker.patch.object(requests, "post", autospec=True, return_value=res_mock)
    return identity_provider, res_json


def test_get_token(identity_provider_patched_post: IdentityProvider):
    idp, tokens = identity_provider_patched_post
    assert idp.get_token(TokenType.ACCESS_TOKEN, False) == tokens["access_token"]


def test_get_token__access_token__request_new(
    identity_provider_patched_post: IdentityProvider,
):
    idp, tokens = identity_provider_patched_post
    assert idp.get_token(TokenType.ACCESS_TOKEN, True) == tokens["access_token"]


def test_get_token__id_token__request_new(
    identity_provider_patched_post: IdentityProvider,
):
    idp, tokens = identity_provider_patched_post
    assert idp.get_token(TokenType.ID_TOKEN, True) == tokens["id_token"]


def test_get_token__refresh_token__request_new(
    identity_provider_patched_post: IdentityProvider,
):
    idp, tokens = identity_provider_patched_post
    assert idp.get_token(TokenType.REFRESH_TOKEN, True) == tokens["refresh_token"]


def test_get_token__invalid_token__request_new(
    identity_provider_patched_post: IdentityProvider,
):
    idp, _ = identity_provider_patched_post
    assert idp.get_token("my_fictional_token", True) == None


def test_get_certs(
    mocker, identity_provider_patched_post: IdentityProvider, cert_idp_json
):
    res_mock = mocker.Mock(wrap=requests.get)
    res_mock.json.return_value = cert_idp_json
    mocker.patch.object(requests, "get", autospec=True, return_value=res_mock)
    idp, _ = identity_provider_patched_post
    res = idp.get_certs()
    assert res == cert_idp_json


def callback(*args):
    pass


def test_run_forever(mocker: MockerFixture, identity_provider: IdentityProvider):
    mocker.patch.object(threading, "_start_new_thread", autospec=True)
    identity_provider._token_inspector_run = True
    identity_provider.run_forever(TokenType.ACCESS_TOKEN, callback)


def test_run_forever__refresh_token(
    mocker: MockerFixture, identity_provider: IdentityProvider
):
    mocker.patch.object(threading, "_start_new_thread", autospec=True)
    assert identity_provider.run_forever(TokenType.REFRESH_TOKEN, callback) == None


def test_verify_token(
    mocker: MockerFixture,
    identity_provider_patched_post: IdentityProvider,
    jwt_access_token,
    cert_idp_json,
):
    # jwt_access_token has already expired, hence jwt.decode will raise
    # an ExpiredSignature exception, which results in idp.verify_token 
    # returning false instead of true. Therfore the behavior of jwt.decode
    # has to be patched.
    mocker.patch.object(jwt, "decode", autospec=True, return_value=True)

    res_mock = mocker.Mock(wrap=requests.get)
    res_mock.json.return_value = cert_idp_json
    mocker.patch.object(requests, "get", autospec=True, return_value=res_mock)
    idp, _ = identity_provider_patched_post
    assert idp.verify_token(jwt_access_token) == True


def test_verify_token__false(
    mocker, identity_provider_patched_post: IdentityProvider, cert_idp_json
):
    jwt_token = "my_token"
    res_mock = mocker.Mock(wrap=requests.get)
    res_mock.json.return_value = cert_idp_json
    mocker.patch.object(requests, "get", autospec=True, return_value=res_mock)
    idp, _ = identity_provider_patched_post
    assert idp.verify_token(jwt_token) == False


def test_authenticate__invalid_grant_type(identity_provider: IdentityProvider):
    identity_provider._grant_type = "invalid_grant_type"
    assert identity_provider._authenticate("KWH") == {}


def test_authenticate__authorization_code_flow(
    mocker: MockerFixture, identity_provider: IdentityProvider, token_bundle_valid
):
    # Mock webbrowser such that it doesnt open one on the host system.
    # Url isn't available, if proxy server is not running.
    mocker.patch.object(webbrowser, "open_new_tab", autospec=True)
    mock_res_init = mocker.Mock(wraps=requests.Response)
    mock_res_init.json.return_value = {
        "redirect_url": "https://kwh40.de",
        "proxy_user_identifier": "s3i:abcd",
        "proxy_secret": "secret",
    }

    mock_res = mocker.Mock(wraps=requests.Response)
    mock_res.json.return_value = token_bundle_valid
    mock_res.text = None

    mock_res_1 = mocker.Mock(wraps=requests.Response)
    mock_res_1.json.return_value = token_bundle_valid
    mock_res_1.text = "token available"
    mocker.patch.object(
        requests,
        "get",
        autospec=True,
        side_effect=[mock_res_init, mock_res, mock_res_1],
    )
    grant_type = "authorization_code_flow"
    identity_provider._grant_type = grant_type
    identity_provider._authenticate("KWH")
    assert identity_provider._token_bundle == token_bundle_valid


def test_refresh_token(mocker, identity_provider: IdentityProvider, token_bundle):
    mock_res = mocker.Mock(wraps=requests.Response)
    mock_res.status_code = 200
    mock_res.json.return_value = token_bundle
    mocker.patch.object(requests, "post", autospec=True, return_value=mock_res)
    identity_provider._refresh_token("my_token", scope="KWH")
    assert identity_provider._token_bundle == token_bundle
