import pytest
from pytest_mock import MockerFixture

from s3i.directory import Directory, pgpy
from s3i.directory import requests as dir_req
from s3i.ditto_manager import requests


@pytest.fixture
def token():
    return "abcd"


@pytest.fixture
def headers(token):
    return {"Content-Type": "application/json", "Authorization": f"Bearer {token}"}


@pytest.fixture
def thing():
    service_type = "push"
    endpoints = ["s3i:abcd"]
    targets = {"endpoints": endpoints, "serviceType": service_type}
    links = [{"target": targets}]
    thing = {"attributes": {"thingStructure": {"links": links}}}
    return thing


@pytest.fixture
def endpoints(thing):
    return thing["attributes"]["thingStructure"]["links"][0]["target"]["endpoints"]


@pytest.fixture
def directory(directory_url, token):
    return Directory(directory_url, token)


def assert_directory_state(directory: Directory, url, token, headers):
    assert directory.ditto_url == url
    assert directory.token == token
    assert directory.ditto_headers == headers


def test_constructor(directory: Directory, directory_url, token, headers):
    assert_directory_state(directory, directory_url, token, headers)


# TODO: Activate test: https://git.rwth-aachen.de/kwh40/s3i/-/issues/26
# def test_queryEndpointService(
#     mocker: MockerFixture, directory: Directory, thing, endpoints
# ):
#     res_mock = mocker.Mock(wraps=requests.Response)
#     res_mock.status_code = 200
#     res_mock.json.return_value = thing
#     mocker.patch.object(requests, "get", autospec=True, return_value=res_mock)

#     service_type = "push"
#     res = directory.queryEndpointService("s3i:abcd", service_type)
#     assert endpoints == res


def test_getPublicKey__str_argument(mocker: MockerFixture, directory: Directory):
    res_mock = mocker.Mock(wraps=requests.Response)
    res_mock.status_code = 200
    res_text = "mykey"
    res_mock.text = res_text
    res_mock.json.return_value = {"public_key": res_text}
    mocker.patch.object(dir_req, "get", autospec=True, return_value=res_mock)
    thing_id = "s3i:abcd"

    res = directory.getPublicKey(thing_id)
    assert res == res_text


def test_getPublicKey__list_argument(mocker: MockerFixture, directory: Directory):
    res_mock = mocker.Mock(wraps=requests.Response)
    res_mock.status_code = 200
    res_text = "mykey"
    res_mock.text = res_text
    res_mock.json.return_value = {"public_key": res_text}
    mocker.patch.object(dir_req, "get", autospec=True, return_value=res_mock)
    thing_ids = ["s3i:abcd"]

    res = directory.getPublicKey(thing_ids)
    assert res == [res_text]


def test_get_publicKey__invalid_argument(directory: Directory):
    directory.getPublicKey(2)


def test_putPublicKey(mocker: MockerFixture, directory: Directory):
    mocker.patch.object(
        pgpy.PGPKey, "from_file", autospec=True, return_value=["mykey", "idontknow"]
    )

    res_mock_req = mocker.Mock(wraps=requests.Response)
    res_mock_req.status_code = 204
    res_json = {"res": "success"}
    res_mock_req.json = res_json
    mocker.patch.object(dir_req, "put", autospec=True, return_value=res_mock_req)

    res = directory.putPublicKey("s3i:abcd", "mykey.asc")
    assert res == res_mock_req
