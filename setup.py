from setuptools import setup, find_packages

setup(
    name="s3i",
    version="0.7.7",
    author="Kompetenzzentrum Wald und Holz 4.0",
    author_email="s3i@kwh40.de",
    url="https://www.kwh40.de/",
    packages=find_packages(),
    include_package_data=True,
    install_requires=["alabaster==0.7.12",
                      "astroid==2.3.3",
                      "certifi==2019.11.28",
                      "cffi<=1.16.0",
                      "chardet==3.0.4",
                      "colorama==0.4.3",
                      "cryptography==42.0.5",
                      "idna==2.8",
                      "isort==4.3.21",
                      "Jinja2==2.10.3",
                      "jsonschema==4.21.1",
                      "lazy-object-proxy==1.4.3",
                      "MarkupSafe==1.1.1",
                      "mccabe==0.6.1",
                      "pika==1.3.2",
                      "pockets==0.9.1",
                      "pycparser==2.19",
                      "Pygments==2.5.2",
                      "PyJWT==1.7.1",
                      "python-benedict==0.21.0",
                      "pyparsing==2.4.5",
                      "pytz==2019.3",
                      "pyrql==0.6.0",
                      "python-keycloak==3.9.0",
                      "requests==2.25.1",
                      "six==1.13.0",
                      "snowballstemmer==2.0.0",
                      "wrapt==1.11.2",
                      "pgpy",
                      "python-gnupg",
                      "websocket-client",
                      "schema==0.7.1",
                      "urllib3==1.26.5"
                      ],
    extras_require={
        "dev": [
            "pynput"
        ]
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)",
        "Operating System :: OS Independent",
    ]
)
