from s3i import IdentityProvider, TokenType

if __name__ == "__main__":
    username = input("[S3I]: Please enter your username: ")
    password = input("[S3I]: Please enter your password: ")
    client = input(
        "[S3I]: Please enter the thing (client) you want to authorize (or leave blank to default to admin client): ")
    if client == "":
        client = "admin-cli"
        client_secret = ""
    else:
        client_secret = input(
            "[S3I]: Please enter the thing's secret (client secret): ")
    scope = input("[S3I]: Please enter the client scope which is requested for the access token and has been assigned to the client (or leave blank): ")
    idp = IdentityProvider(grant_type="password", client_id=client, username=username, password=password,   
                           client_secret=client_secret, realm="KWH", identity_provider_url="https://idp.s3i.vswf.dev/")
    print("[S3I]: Access token: \n", idp.get_token(TokenType.ACCESS_TOKEN, scope=scope))
