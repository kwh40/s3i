from s3i import IdentityProvider, Directory, BrokerAMQP, ServiceReply, TokenType, APP_LOGGER, setup_logger, ServiceRequest, S3IBMessageError
import uuid
import json
import base64
import os
import jwt
from jwt import DecodeError, ExpiredSignatureError, InvalidTokenError
from jwt.algorithms import RSAPSSAlgorithm
import asyncio


def callback(ch, method, properties, body):
    """
    This function will be called and executed every time that S3I Broker delivers messages to the consuming application (DT). \n
    It passes a request to service function getProductionDataService() and sends a service reply to the requester.

    Args:
        ch: channel used during consuming, which binds productor and consumer \n
        method: „method“ contains meta information regarding message delivery: It provides delivery flags, redelivery flags, and a routing key that can be used to acknowledge the message. \n
        properties: properties of queue and exchange which are used during consuming \n
        body: content of message  
    """
    if isinstance(body, bytes):
        try:
            service_request = ServiceRequest(base_msg=body)
        except S3IBMessageError as e:
            APP_LOGGER.error(e)
        else:
            APP_LOGGER.info("Get a S3I-B {} with the service Type{}".format(service_request.base_msg["messageType"],
                                                                            service_request.base_msg["serviceType"]))
            results = getProductionDataService(service_request.base_msg["serviceType"])
            """
            Prepare a service reply 
            """
            APP_LOGGER.info("Service function is executed. Preparing message to reply")
            serv_reply = ServiceReply()
            serv_reply.fillServiceReply(
                sender="s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63",
                receivers=[service_request.base_msg["sender"]],
                message_id="s3i:" + str(uuid.uuid4()),
                service_type=service_request.base_msg["serviceType"],
                replying_to_msg=service_request.base_msg["identifier"],
                results=results
            )

            """
            send service reply to the requester
            """
            APP_LOGGER.info("Attempts to send a service reply back")
            broker.send(
                endpoints=[service_request.base_msg["replyToEndpoint"]],
                msg=json.dumps(serv_reply.base_msg)
            )


def getProductionDataService(service_type):
    """
    Simulates a get production data service and returns a BASE64-encoded StanForD 2010 HPR production data file

    Args:
        service_type(str): service type
    """

    if service_type == "fml40::ProvidesProductionData/getProductionData":
        APP_LOGGER.info("Forest machine collects production data...")
        """
        for this demo, a dummy demo3.hpr has already been prepared for the service reply. 
        """
        cwd = os.path.dirname(__file__)
        path = os.path.join(cwd, "demo_send_data", "demo3.hpr")

        """
        convert the data to BASE64
        """

        with open(path, "rb") as f:
            base64_data = base64.b64encode(f.read())
            data = base64_data.decode()

        """
        store the production data in a dict
        """
        result = dict()
        result["productionData"] = data
        return result
    else:
        return None


def authenticate():
    idp = IdentityProvider(
        grant_type="client_credentials",
        identity_provider_url="https://idp.s3i.vswf.dev/",
        realm='KWH',
        client_id="s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63",
        client_secret="950dc82f-1bac-4852-a3ee-1e62b22e258e",
    )
    APP_LOGGER.info("Client credentials are sent to S3I IdentityProvider.")
    return idp


def get_token(idp):
    APP_LOGGER.info("Token obtained.")
    return idp.get_token(TokenType.ACCESS_TOKEN)


def verify_token(idp, token):
    idp_keys = idp.get_certs()["keys"]
    public_key = ""
    for key in idp_keys:
        if key["alg"] == "RS256" and key["kty"] == "RSA" and key["use"] == "sig":
            public_key = RSAPSSAlgorithm.from_jwk(json.dumps(key))
            break
    try:
        """decode the access token with public key that was searched above"""
        jwt.decode(
            jwt=token,
            key=public_key,
            algorithms=["RS256"],
            audience="rabbitmq"
        )
    except (DecodeError, ExpiredSignatureError, InvalidTokenError):
        return False
    else:
        return True


def connect_to_broker(token, loop):
    APP_LOGGER.info("Connecting to the broker endpoint {}".format(harvester_service_endpoint))
    br = BrokerAMQP(
        token=token,
        endpoint=harvester_service_endpoint,
        callback=callback,
        loop=loop
    )
    br.connect()
    return br


if __name__ == "__main__":

    setup_logger("[S3I][Demo3][ForestMachine]")

    """
    Step 1: Forest machine tries to authenticate himself against S3I IdentityProvider, in order to obtain an access token.
    In this Step, the grant type is selected as client credentials
    """
    APP_LOGGER.info("Automatically logged in as forest machine.")
    s3i_identity_provider = authenticate()
    access_token = get_token(s3i_identity_provider)

    """
    Both ThingId and S3I endpoint of harvester are known 
    """

    harvester_id = "s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63"
    harvester_service_endpoint = "s3ib://s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63"

    """
    Step 2: Authentication with access token (JWT) at S3I Broker and listen to the callback function
    """
    loop = asyncio.get_event_loop()
    broker = connect_to_broker(token=access_token, loop=loop)
    """
    Step 3: Enable the loop 
    """
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        APP_LOGGER.info("Disconnect from S3I")
        loop.close()

