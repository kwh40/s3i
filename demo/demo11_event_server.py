import os
import time
from sys import stdout

from pynput import keyboard
import ml

class bcolors:
    """colors for the console log"""
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


harvester_data_model = {
    "thingId": "s3i:7f08eb3a-0b01-4466-bd0b-438c3a1e47e5",
    "policyId": "s3i:7f08eb3a-0b01-4466-bd0b-438c3a1e47e5",
    "attributes": {
        "class": "ml40::Thing",
        "name": "Testing-Harvester for EventSystem",
        "roles": [{"class": "fml40::Harvester"}],
        "features": [
            {
                "class": "ml40::Composite",
                "targets": [
                    {
                        "class": "ml40::Thing",
                        "name": "Motor",
                        "roles": [{"class": "ml40::Engine"}],
                        "features": [
                            {
                                "class": "ml40::RotationalSpeed",
                                "rpm": 3000
                            }
                        ]
                    },
                    {
                        "class": "ml40::Thing",
                        "name": "Kran",
                        "roles": [{"class": "ml40::Crane"}],
                        "features": [
                            {
                                "class": "ml40::Composite",
                                "targets": [
                                    {
                                        "class": "ml40::Thing",
                                        "name": "Harvesterkopf",
                                        "roles": [{"class": "fml40::HarvestingHead"}],
                                        "features": [
                                            {
                                                "class": "fml40::Grabs"
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "class": "ml40::Thing",
                        "name": "Bordcomputer",
                        "roles": [{"class": "ml40::MachineUI"}]
                    }
                ]
            },
            {
                "class": "fml40::ProvidesProductionData"
            },
            {
                "class": "fml40::AcceptsFellingJobs"
            },
            {
                "class": "fml40::Harvests"
            },
            {
                "class": "fml40::ManagesJobs"
            },
            {
                "class": "fml40::AcceptsProximityAlert"
            },
            {
                "class": "ml40::Location",
                "longitude": 6.249276,
                "latitude": 50.808796
            },
            {
                "class": "ml40::Shared",
                "targets": [
                    {
                        "class": "ml40::Thing",
                        "name": "Winde",
                        "roles": [{"class": "fml40::Winch"}]
                    }
                ]
            }
        ]
    }
}
HARVESTER_ENDPOINT = "s3ib://s3i:7f08eb3a-0b01-4466-bd0b-438c3a1e47e5"
HARVESTER_CLIENT_ID = "s3i:7f08eb3a-0b01-4466-bd0b-438c3a1e47e5"
HARVESTER_SECRET = "07d65f82-b5c8-4dff-af69-45d8cd084be2"

#ml.setup_logger("event_server")
thing = ml.create_thing(model=harvester_data_model, grant_type="client_credentials",
                        secret=HARVESTER_SECRET, is_broker_rest=False,
                        is_broker=True, is_repo=False)


def simulate_func():
    location = thing.features["ml40::Location"]
    motor_speed = thing.features["ml40::Composite"].targets["Motor"].features["ml40::RotationalSpeed"]
    decrease_rpm = 30
    while True:
        if motor_speed.rpm > decrease_rpm:
            motor_speed.rpm -= decrease_rpm
            location.longitude += 0.1
            location.latitude -= 0.1
            stdout.write("\r[S³I] current harvester rpm: " + bcolors.OKGREEN + "%d" % motor_speed.rpm +
                         bcolors.ENDC + " || current harvester location: " + bcolors.OKBLUE + "longitude: " +
                         "%f" % location.longitude + " latitude: %f" % location.latitude + bcolors.ENDC)
            stdout.flush()
        time.sleep(2)



def on_keyboard_press(key):
    """
    Callback function which is called when the input key is pressed

    :param key: key of keyboard
    Returns:
        None
    """
    if key == keyboard.Key.alt_r:
        os._exit(0)


if __name__ == "__main__":
    print("=======================================================================================")
    print("Welcome to S³I EventSystem Demo. Launching the harvester gSFL model")
    print("=======================================================================================")
    #keyboard_listener = keyboard.Listener(on_press=on_keyboard_press)
    #keyboard_listener.start()

    thing.add_user_def(func=simulate_func)
    thing.run_forever()