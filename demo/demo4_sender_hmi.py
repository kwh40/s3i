from s3i import IdentityProvider, Directory, BrokerAMQP, UserMessage, Key, TokenType, APP_LOGGER, setup_logger
import uuid
import json
import base64
import os
import jwt
from jwt import DecodeError, ExpiredSignatureError, InvalidTokenError
from jwt.algorithms import RSAPSSAlgorithm
import asyncio
writeToFile = False
RECEIVER = "Schmitz"


def authenticate(username, password):
    idp = IdentityProvider(
        grant_type='password',
        identity_provider_url="https://idp.s3i.vswf.dev/",
        realm='KWH',
        client_id="s3i:2aafd97c-ff05-42b6-8e4d-e492330ec959",
        client_secret="f584c77e-e0b6-4736-831b-ccf47ab23a65",
        username=username,
        password=password
    )
    APP_LOGGER.info("Username and password are sent to S3I IdentityProvider.")
    return idp


def get_token(idp):
    APP_LOGGER.info("Token obtained.")
    return idp.get_token(TokenType.ACCESS_TOKEN)


def verify_token(idp, token):
    idp_keys = idp.get_certs()["keys"]
    public_key = ""
    for key in idp_keys:
        if key["alg"] == "RS256" and key["kty"] == "RSA" and key["use"] == "sig":
            public_key = RSAPSSAlgorithm.from_jwk(json.dumps(key))
            break
    try:
        """decode the access token with public key that was searched above"""
        jwt.decode(
            jwt=token,
            key=public_key,
            algorithms=["RS256"],
            audience="rabbitmq"
        )
    except (DecodeError, ExpiredSignatureError, InvalidTokenError):
        return False
    else:
        return True


def search_hmi_in_dir(token):
    s3i_dir = Directory(s3i_dir_url="https://dir.s3i.vswf.dev/api/2/", token=token)
    dt_receiver = s3i_dir.queryAttributeBased(
        key='thingStructure/links/target/values/value',
        value="Schmitz")  # data model of person's dt is known previously
    APP_LOGGER.info("Search the default HMI of the receiver Mr. Schmitz")
    receiver_id = dt_receiver[0]["thingId"]
    receiver_hmi_id = dt_receiver[0]["attributes"]["defaultHMI"]
    APP_LOGGER.info("The HMI (id: {}) of Mr. Schmitz (id: {}) is found".format(receiver_hmi_id, receiver_id))
    hmi_schmitz = s3i_dir.queryThingIDBased(thingID=receiver_hmi_id)
    hmi_schmitz_endpoint_list = hmi_schmitz["attributes"]["defaultEndpoints"]
    return receiver_hmi_id, hmi_schmitz_endpoint_list


def connect_to_broker(token, loop):
    APP_LOGGER.info("Connecting to the broker endpoint at {}".format(sender_endpoint))
    br = BrokerAMQP(
        token=token,
        endpoint=sender_endpoint,
        callback=None,
        loop=loop
    )
    br.connect()
    return br


def encrypt_and_sign_msg():
    APP_LOGGER.info("Prepare the message which will be sent to Mr. Schmitz")
    subject = input('[S3I]: Please enter the message subject: \n')
    text = input('[S3I]: Please enter the text: \n')
    msg_uuid = "s3i:" + str(uuid.uuid4())
    filename_list = list(map(str, input(
        "[S3I]: Please enter the name of file that you want to send: ").split()))
    attachments = list()

    for i in range(len(filename_list)):
        attachment_dict = dict()
        attachment_dict["filename"] = filename_list[i]
        path_attachments = os.path.join(
            os.path.dirname(__file__), "demo_send_data", filename_list[i])
        with open(path_attachments, "rb") as f:
            # encode an attachment file to BASE64 bytes
            base64_data = base64.b64encode(f.read())
            data = base64_data.decode()  # convert byte to str
            attachment_dict["data"] = data
            attachments.append(attachment_dict)
            f.close()

    uMsg = UserMessage()
    uMsg.fillUserMessage(
        sender=sender,
        receivers=[receiver_hmi_id],
        reply_to_endpoint=sender_endpoint,
        subject=subject,
        text=text,
        message_id=msg_uuid,
        attachments=attachments
    )

    """
    Signing the message
    """
    fp = os.path.dirname(os.path.abspath(__file__))
    fp = os.path.join(fp, "..", "key")
    pgpKey_sec = Key(path_demo_dir=fp, filename="waldbesitzer_sec.asc")
    uMsg.convert_base_to_pgp()
    with pgpKey_sec.key.unlock("waldbesitzer") as ukey:
        signed_pgp_message = uMsg.sign(ukey, msg_pgp=uMsg.pgp_msg)

    APP_LOGGER.info("Signed the message with the private key stored in {}".format(fp))

    """
    Encryption based on RFC 4880
    """
    s3i_dir = Directory(s3i_dir_url="https://dir.s3i.vswf.dev/api/2/", token=access_token)
    key_str = s3i_dir.getPublicKey(receiver_hmi_id)
    pub_key = Key(key_str=key_str).key
    uMsg.encrypt(pub_key, uMsg.pgp_msg)

    """ 
    Write to file only if global flag writeToFile is set
    """
    if writeToFile:
        f = open("pgpMessage_demo4.txt", "w")
        f.write(uMsg.pgp_msg.__str__())
        f.close()

    return uMsg.pgp_msg


if __name__ == "__main__":

    setup_logger("[S3I][Demo4][Sender]")

    """
    Step 1: Sender tries to authenticate himself against S3I IdentityProvider, in order to obtain an access token.
    In this Step, the grant type is selected as password which is a simply way to get a access token. 
    In a subsequent version, the grant type as authorization code flow will be implemented 
    """

    APP_LOGGER.info("Please log in as forest owner (franz_maier).")
    username = input('[S3I]: Please enter the username: \n')
    password = input('[S3I]: Please enter the password: \n')
    s3i_identity_provider = authenticate(username, password)
    access_token = get_token(s3i_identity_provider)

    """
    Step 2: Decode the access token
    """
    if verify_token(s3i_identity_provider, access_token):
        APP_LOGGER.info("Token is valid")
    else:
        APP_LOGGER.error("Invalid token")

    """
    Step 3: Search the default HMI of the receiver 
    """
    receiver_hmi_id, receiver_hmi_endpoints = search_hmi_in_dir(access_token)

    """
    The endpoint and thing id of sender's HMI are known
    """

    sender_endpoint = "s3ib://s3i:2aafd97c-ff05-42b6-8e4d-e492330ec959"
    sender = "s3i:2aafd97c-ff05-42b6-8e4d-e492330ec959"

    """
    Step 4: Search receiver's s3i endpoint with "s3ibs://..."
    """
    receiver_s3ib_endpoint = ""
    for i in receiver_hmi_endpoints:
        if "s3ibs://" in i:
            receiver_s3ib_endpoint = i
            break

    """
    Step 5: Authentication with access token (JWT) at S3I Broker
    """
    loop = asyncio.get_event_loop()
    broker = connect_to_broker(token=access_token, loop=loop)

    """
    Step 6: prepare the message
    """
    encrypted_and_signed_message = encrypt_and_sign_msg()

    """
    Step 7: Send the message
    """
    broker.add_on_channel_open_callback(
        broker.send,
        True,
        [receiver_s3ib_endpoint],
        encrypted_and_signed_message.__str__()
    )

    """
    Step 8: Enable the loop 
    """
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        APP_LOGGER.info("Disconnect from S3I")
        loop.close()
