from s3i import IdentityProvider, Config, Directory, TokenType
import json


if __name__ == "__main__":
    print("************************************************************")
    print("**Welcome to demo: add S3I Directory entry******************")
    print("************************************************************")
    """query access token of existing user. """
    username = input(
        "[S3I]: Please enter an existing username: ")
    password = input(
        "[S3I]: Please enter the corresponding password: ")
    client_id = input(
        "[S3I]: Please enter your available client id (obtained from demo 6): "
    )
    client_secret = input(
        "[S3I]: Please enter the respective client secret (obtained from demo 6): "
    )
    idp_token = IdentityProvider(grant_type="password", client_id=client_id,
                                 client_secret=client_secret, realm="KWH", identity_provider_url="https://idp.s3i.vswf.dev/",
                                 username=username, password=password)
    token = idp_token.get_token(TokenType.ACCESS_TOKEN)
    print("[S3I]: token obtained")

    """config instance"""
    s3i_config = Config(server_url="https://config.s3i.vswf.dev/", token=token)
    s3i_dir = Directory(s3i_dir_url="https://dir.s3i.vswf.dev/api/2/", token=token)

    """step 1: create a directory entry"""
    print("[S3I]: Create a S3I Thing with an empty directory entry")
    create_thing_resp = s3i_config.create_thing()
    print("[S3I]: Directory entry created with HTTP Response code {} with id: {} and secret: {}".format(
        create_thing_resp.status_code, create_thing_resp.json()["identifier"], create_thing_resp.json()["secret"]))

    """step 2: Update the directory entry"""
    print("[S3I]: Update the directory entry")
    name = input("[S3I]: Please enter a name for the directory entry: ")
    role = input("[S3I]: Please define a role for the directory entry (e.g., fml40::Harvester): ")
    attribute = input("[S3I]: Please define a suitable property for the directory entry (e.g., ml40::Weight): ")
    payload = s3i_dir.queryThingIDBased(thingID=create_thing_resp.json()["identifier"])
    payload["attributes"]["name"] = name
    payload["attributes"]["thingStructure"] = {
        "class": "ml40::Thing",
        "links": [
            {
                "association": "roles",
                "target": {
                    "class": role
                }
            },
            {
                "association": "features",
                "target": {
                    "class": attribute
                }
            }
        ]
    }
    print("[S3I]: The updated directory entry: {}".format(json.dumps(payload, indent=2)))
    s3i_dir.updateThingIDBased(
        thingID=create_thing_resp.json()["identifier"],
        payload=payload
    )

    """step 3: Delete the created directory entry"""
    s3i_config.delete_thing(thing_id=create_thing_resp.json()["identifier"])
    print("[S3I]: The created directory entry is deleted")

