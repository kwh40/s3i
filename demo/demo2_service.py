from s3i import IdentityProvider, Directory, BrokerAMQP, ServiceReply, ServiceRequest, TokenType, setup_logger, APP_LOGGER, S3IBMessageError
import uuid
import json
import jwt
import base64
import os
import asyncio
from jwt import DecodeError, ExpiredSignatureError, InvalidTokenError
from jwt.algorithms import RSAPSSAlgorithm


def callback(ch, method, properties, body):
    """
    This function will be called and executed every time that S3I Broker delivers messages to the consuming application (Service). \n
    It passes a service request message to the service function calculateStockService() and sends a service reply with the result to the requester.

    Args:
        ch: channel used during consuming, which binds productor and consumer \n
        method: „method“ contains meta information regarding message delivery: It provides delivery flags, redelivery flags, and a routing key that can be used to acknowledge the message. \n
        properties: properties of queue and exchange which are used during consuming \n
        body: content of message 
    """

    if isinstance(body, bytes):
        try:
            service_request = ServiceRequest(base_msg=body)
        except S3IBMessageError as e:
            APP_LOGGER.error(e)
        else:
            APP_LOGGER.info("Get a S3I-B {} with the service type {}".format(
               service_request.base_msg["messageType"], service_request.base_msg["serviceType"]))

            """
            get BASE64-encoded parameters 
            """
            decoded_parameters = service_request.base_msg["parameters"]["surface"]

            """
            call the service function
            """
            results = calculateStockService(
                service_request.base_msg["serviceType"], decoded_parameters)

            '''
            configure S3I-B service reply message 
            '''
            ser_reply = ServiceReply()
            ser_reply.fillServiceReply(
                sender="s3i:ab0717b0-e025-4344-8598-bcca1d3d5d47",
                receivers=[service_request.base_msg["sender"]],
                service_type=service_request.base_msg["serviceType"],
                results=results,
                message_id="s3i:" + str(uuid.uuid4()),
                replying_to_msg=service_request.base_msg["identifier"]
            )
            '''
            send the S3I-B service reply message 
            '''
            broker.send(
                endpoints=[service_request.base_msg["replyToEndpoint"]],
                msg=json.dumps(ser_reply.base_msg))


def calculateStockService(serviceType, parameters):
    """
    Simulate calculate stock service

    Args:
        parameters(str): decoded parameters value \n
        serviceType(str): the type of service called by the requester

    Returns:
        result of service(str)
    """
    results = ""
    if serviceType == "calculateStock":
        if parameters is not None:
            APP_LOGGER.info("Service is calculating")
            results = {"stock": "123.4"}
    else:
        results = None

    return results


def authenticate():
    idp = IdentityProvider(
        grant_type="client_credentials",
        identity_provider_url="https://idp.s3i.vswf.dev/",
        realm='KWH',
        client_id="s3i:ab0717b0-e025-4344-8598-bcca1d3d5d47",
        client_secret="cd23353e-10df-4f9c-864a-b65b2e8393c0",
    )
    APP_LOGGER.info("Client credentials are sent to S3I IdentityProvider.")
    return idp


def get_token(idp):
    APP_LOGGER.info("Token obtained.")
    return idp.get_token(TokenType.ACCESS_TOKEN)


def verify_token(idp, token):
    idp_keys = idp.get_certs()["keys"]
    public_key = ""
    for key in idp_keys:
        if key["alg"] == "RS256" and key["kty"] == "RSA" and key["use"] == "sig":
            public_key = RSAPSSAlgorithm.from_jwk(json.dumps(key))
            break
    try:
        """decode the access token with public key that was searched above"""
        jwt.decode(
            jwt=token,
            key=public_key,
            algorithms=["RS256"],
            audience="rabbitmq"
        )
    except (DecodeError, ExpiredSignatureError, InvalidTokenError):
        return False
    else:
        return True


def connect_to_broker(token, loop):
    APP_LOGGER.info("Connecting to the broker endpoint {}".format(service_endpoint))
    br = BrokerAMQP(
        token=token,
        endpoint=service_endpoint,
        callback=callback,
        loop=loop
    )
    br.connect()
    return br


if __name__ == "__main__":
    setup_logger("[S3I][Demo2][Service]")
    """
    Step 1: Receiver tries to authenticate himself against S3I IdentityProvider, in order to obtain an access token.
    In this Step, the grant type is selected as client credentials 
    """
    APP_LOGGER.info("Automatically logged in as calculate service.")
    s3i_identity_provider = authenticate()
    access_token = get_token(s3i_identity_provider)

    """
    Step 2: Decode the access token
    """
    if verify_token(s3i_identity_provider, access_token):
        APP_LOGGER.info("Token is valid")
    else:
        APP_LOGGER.error("Invalid token")

    """
    The s3i endpoint of the service is known 
    """
    service_endpoint = "s3ib://s3i:ab0717b0-e025-4344-8598-bcca1d3d5d47"

    '''
    Step 3: Authentication with access token (JWT) at S3I Broker and listen to the callback function 
    '''
    loop = asyncio.get_event_loop()
    broker = connect_to_broker(token=access_token, loop=loop)

    """
    Step 4: Enable the loop 
    """
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        APP_LOGGER.info("Disconnect from S3I")
        loop.close()
