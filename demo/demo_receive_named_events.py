from s3i import IdentityProvider, Directory, BrokerAMQP, EventMessage, TokenType, setup_logger, APP_LOGGER, S3IBMessageError
import json
import jwt
import base64
import os
import asyncio
from jwt import DecodeError, ExpiredSignatureError, InvalidTokenError
from jwt.algorithms import RSAPSSAlgorithm

schmitz_hmi_id = "s3i:6f58e045-fd30-496d-b519-a0b966f1ab01"
schmitz_hmi_secret = "475431fd-2c6d-4cae-bdfa-87226fff0cef"

def callback(ch, method, properties, body):
    """
    This function will be called and executed every time that S3I Broker delivers messages to the consuming application (HMI). \n
    The subject and text included in the user message will be parsed and printed. \n
    Attachments will be decoded and stored in folder ../src/demo_received_data.

    Args:
        ch: channel used during consuming, which binds producer and consumer \n
        method: „method“ contains meta information regarding message delivery: It provides delivery flags, redelivery flags, and a routing key that can be used to acknowledge the message. \n
        properties: properties of queue and exchange which are used during consuming \n
        body: content of message
    """
    if isinstance(body, bytes):
        try:
            eMsg = EventMessage(base_msg=body)
        except S3IBMessageError as e:
            APP_LOGGER.error(e)
        else:
            APP_LOGGER.info("[S3I]: A new event message is obtained")
            APP_LOGGER.info("[S3I]: Message: {}".format(eMsg.base_msg['content']))
            """For the scope of this demo, we can unsubscribe after receiving an event message"""

            """The topic, composed by the sending in the format sendingThingId.topicName is known here"""
            topic = "s3i:2aafd97c-ff05-42b6-8e4d-e492330ec959.TestEvent"
            broker.add_on_channel_open_callback(broker.unsubscribe_topic, True, topic)


def authenticate(username, password):
    idp = IdentityProvider(
        grant_type='password',
        identity_provider_url="https://idp.s3i.vswf.dev/",
        realm='KWH',
        client_id=schmitz_hmi_id,
        client_secret=schmitz_hmi_secret,
        username=username,
        password=password
    )
    APP_LOGGER.info("[S3I]: Username and password are sent to S3I IdentityProvider.")
    return idp


def get_token(idp):
    APP_LOGGER.info("[S3I]: Token obtained.")
    return idp.get_token(TokenType.ACCESS_TOKEN)


def verify_token(idp, token):
    idp_keys = idp.get_certs()["keys"]
    public_key = ""
    for key in idp_keys:
        if key["alg"] == "RS256" and key["kty"] == "RSA" and key["use"] == "sig":
            public_key = RSAPSSAlgorithm.from_jwk(json.dumps(key))
            break
    try:
        """decode the access token with public key that was searched above"""
        jwt.decode(
            jwt=token,
            key=public_key,
            algorithms=["RS256"],
            audience="rabbitmq"
        )
    except (DecodeError, ExpiredSignatureError, InvalidTokenError):
        return False
    else:
        return True


def connect_to_broker(token, loop):
    APP_LOGGER.info("[S3I]: Connecting to the broker endpoint at {}".format(hmi_schmitz_endpoint))
    br = BrokerAMQP(
        token=token,
        endpoint=hmi_schmitz_endpoint,
        callback=callback,
        loop=loop,
        listenToEvents=True
    )
    br.connect()

    """The topic, composed by the sending in the format sendingThingId.topicName is known here"""
    topic = "s3i:2aafd97c-ff05-42b6-8e4d-e492330ec959.TestEvent"
    br.add_on_channel_open_callback(br.subscribe_topic,True, topic)
    return br


if __name__ == "__main__":

    setup_logger("[Demo1][Receiver]")
    """
    Step 1: Receiver tries to authenticate himself against S3I IdentityProvider, in order to obtain an access token.
    In this Step, the grant type is selected as password which is a simply way to get a access token. 
    In a subsequent version, the grant type as authorization code flow will be implemented 
    """
    APP_LOGGER.info("[S3I]: Please log in.")
    username = input('[S3I]: Please enter the username: \n')
    password = input('[S3I]: Please enter the password: \n')
    s3i_identity_provider = authenticate(username, password)
    access_token = get_token(s3i_identity_provider)

    """
    Step 2: Decode the access token
    """
    if verify_token(s3i_identity_provider, access_token):
        APP_LOGGER.info("[S3I]: Token is valid")
    else:
        APP_LOGGER.error("[S3I]: Invalid token")

    """
    The endpoint of schmitz's HMI is known
    """
    hmi_schmitz_endpoint = "s3ibs://s3i:6f58e045-fd30-496d-b519-a0b966f1ab01"

    """
    Step 3: Authentication with access token (JWT) at S3I Broker and listen to the callback function 
    """
    loop = asyncio.get_event_loop()
    broker = connect_to_broker(token=access_token, loop=loop)

    """
    Step 4: Enable the loop 
    """
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        APP_LOGGER.info("[S3I]: Disconnect from S3I")
        loop.close()
