import asyncio
from s3i import IdentityProvider, Directory, BrokerAMQP, UserMessage, TokenType, APP_LOGGER, setup_logger
from s3i.event_system import NamedEvent
import json
import jwt
from jwt import DecodeError, ExpiredSignatureError, InvalidTokenError
from jwt.algorithms import RSAPSSAlgorithm


def authenticate(username, password):
    idp = IdentityProvider(
        grant_type='password',
        identity_provider_url="https://idp.s3i.vswf.dev/",
        realm='KWH',
        client_id="s3i:2aafd97c-ff05-42b6-8e4d-e492330ec959",
        client_secret="f584c77e-e0b6-4736-831b-ccf47ab23a65",
        username=username,
        password=password
    )
    APP_LOGGER.info("Username and password are sent to S3I IdentityProvider.")
    return idp


def get_token(idp):
    APP_LOGGER.info("Token obtained.")
    return idp.get_token(TokenType.ACCESS_TOKEN)


def verify_token(idp, token):
    idp_keys = idp.get_certs()["keys"]
    public_key = ""
    for key in idp_keys:
        if key["alg"] == "RS256" and key["kty"] == "RSA" and key["use"] == "sig":
            public_key = RSAPSSAlgorithm.from_jwk(json.dumps(key))
            break
    try:
        """decode the access token with public key that was searched above"""
        jwt.decode(
            jwt=token,
            key=public_key,
            algorithms=["RS256"],
            audience="rabbitmq"
        )
    except (DecodeError, ExpiredSignatureError, InvalidTokenError):
        return False
    else:
        return True


def connect_to_broker(token, loop):
    APP_LOGGER.info("Connecting to the broker endpoint at {}".format(sender_endpoint))
    br = BrokerAMQP(
        token=token,
        endpoint=sender_endpoint,
        callback=None,
        loop=loop
    )
    br.connect()
    return br


def prepare_msg():
    APP_LOGGER.info("Prepare the message which will be published to the Topic <TestEvent>")
    text = input('[S3I]: Please enter the text: \n')
    s3iTopic = s3i_identity_provider.client_id + "." + "TestEvent"
    "The jsonSchema for the Event content. Keys and values can be changed accordingly"
    jsonSchema = {"content": "String"}
    namedEvent = NamedEvent(topic=s3iTopic, json_schema=jsonSchema)
    content = {"content": text}
    namedEvent.generate_event_msg(content)
    eventMsg = namedEvent.msg
    return eventMsg, s3iTopic


if __name__ == "__main__":

    setup_logger("[S3I][Demo1][Sender]")

    """
    Step 1: Sender tries to authenticate himself against S3I IdentityProvider, in order to obtain an access token.
    In this Step, the grant type is selected as password which is a simply way to get a access token. 
    In a subsequent version, the grant type as authorization code flow will be implemented 
    """
    APP_LOGGER.info("Please log in.")
    username = input('[S3I]: Please enter the username: \n')
    password = input('[S3I]: Please enter the password: \n')
    s3i_identity_provider = authenticate(username, password)
    access_token = get_token(s3i_identity_provider)

    """
    Step 2: Decode the access token
    """
    if verify_token(s3i_identity_provider, access_token):
        APP_LOGGER.info("Token is valid")
    else:
        APP_LOGGER.error("Invalid token")

    """
    The endpoint and thing id of sender's HMI are known
    """
    sender_endpoint = "s3ib://s3i:2aafd97c-ff05-42b6-8e4d-e492330ec959"
    sender = "s3i:2aafd97c-ff05-42b6-8e4d-e492330ec959"


    """
    Step 3: Authentication with access token (JWT) at S3I Broker
    """
    loop = asyncio.get_event_loop()
    broker = connect_to_broker(token=access_token, loop=loop)

    """
    Step 4: prepare the event message
    """
    event_message, topic = prepare_msg()

    """
    Step 7: send the message
    """
    broker.add_on_channel_open_callback(
        broker.publish_event,
        True,
        json.dumps(event_message),
        topic
    )

    """
    Step 8: Enable the loop 
    """
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        APP_LOGGER.info("Disconnect from S3I")
        loop.close()
