from s3i import IdentityProvider, Directory, BrokerAMQP, ServiceRequest, ServiceReply, S3IBMessageError, TokenType, APP_LOGGER, setup_logger
import json
import uuid
import base64
import os
import jwt
import asyncio
from jwt import DecodeError, ExpiredSignatureError, InvalidTokenError
from jwt.algorithms import RSAPSSAlgorithm


def callback(ch, method, properties, body):
    """
    This function will be called and executed every time that S3I Broker delivers messages to the consuming application (DT). \n
    It waits for a service reply and then stores the replied production data in ../src/demo_received_data. 


    Args:
        ch: channel used during consuming, which binds productor and consumer \n
        method: „method“ contains meta information regarding message delivery: It provides delivery flags, redelivery flags, and a routing key that can be used to acknowledge the message. \n
        properties: properties of queue and exchange which are used during consuming \n
        body: content of message 
    """
    if isinstance(body, bytes):
        try:
            service_reply = ServiceReply(base_msg=body)
        except S3IBMessageError as e:
            APP_LOGGER.error(e)
        else:
            """
            parse the received BASE64-encoded production data and write it in a file
            """
            cwd = os.path.dirname(__file__)
            path = os.path.join(cwd, "demo_received_data", "Productiondata.hpr")
            results = service_reply.base_msg["results"]
            for key in results.keys():
                decode = base64.b64decode(results[key])
                file = open(path, 'wb')
                file.write(decode)
                file.close()

            APP_LOGGER.info("Service reply is received and production data stored in {}".format(path))


def authenticate():
    idp = IdentityProvider(
        grant_type="client_credentials",
        identity_provider_url="https://idp.s3i.vswf.dev/",
        realm='KWH',
        client_id="s3i:2073c475-fee5-463d-bce1-f702bb06f899",
        client_secret="2a5bd2a0-0aa0-4276-92ce-ebd3dc9813a0",
    )
    APP_LOGGER.info("Client credentials are sent to S3I IdentityProvider.")
    return idp


def get_token(idp):
    APP_LOGGER.info("Token obtained.")
    return idp.get_token(TokenType.ACCESS_TOKEN)


def verify_token(idp, token):
    idp_keys = idp.get_certs()["keys"]
    public_key = ""
    for key in idp_keys:
        if key["alg"] == "RS256" and key["kty"] == "RSA" and key["use"] == "sig":
            public_key = RSAPSSAlgorithm.from_jwk(json.dumps(key))
            break
    try:
        """decode the access token with public key that was searched above"""
        jwt.decode(
            jwt=token,
            key=public_key,
            algorithms=["RS256"],
            audience="rabbitmq"
        )
    except (DecodeError, ExpiredSignatureError, InvalidTokenError):
        return False
    else:
        return True


def search_harvester_in_idp():
    """
    Search the ID and all endpoints of harvester
    """
    s3i_dir = Directory(
        s3i_dir_url="https://dir.s3i.vswf.dev/api/2/", token=access_token)
    dt_harvester = s3i_dir.queryAttributeBased(
        key="name", value="gSFL Harvester")[0]
    dt_harvester_id = dt_harvester["thingId"]
    dt_harvester_endpoints = dt_harvester["attributes"]["allEndpoints"]
    return dt_harvester_id, dt_harvester_endpoints


def connect_to_broker(token, loop):
    APP_LOGGER.info("Connecting to the broker endpoint {}".format(sender_endpoint))
    br = BrokerAMQP(
        token=token,
        endpoint=sender_endpoint,
        callback=callback,
        loop=loop
    )
    br.connect()
    return br


def prepare_service_request():
    msg_uuid = "s3i:" + str(uuid.uuid4())
    serv_req = ServiceRequest()
    serv_req.fillServiceRequest(
        sender=sender,
        receivers=[harvester_id],
        reply_to_endpoint=sender_endpoint,
        service_type="fml40::ProvidesProductionData/getProductionData",
        message_id=msg_uuid
    )
    return serv_req


if __name__ == "__main__":

    setup_logger("[S3I][Demo3][Sawmill]")

    """
    Step 1: Receiver tries to authenticate himself against S3I IdentityProvider, in order to obtain an access token.
    In this Step, the grant type is selected as client credentials
    """
    APP_LOGGER.info("Automatically logged in as sawmill.")
    s3i_identity_provider = authenticate()
    access_token = get_token(s3i_identity_provider)

    """
    Step 2: Decode the access token
    """
    if verify_token(s3i_identity_provider, access_token):
        APP_LOGGER.info("Token is valid")
    else:
        APP_LOGGER.error("Invalid token")

    """
    Step 3: Search the id and endpoint of harvester in S3I Directory 
    """
    harvester_id, harvester_endpoints = search_harvester_in_idp()
    harvester_s3i_endpoint = ""
    for endpoint in harvester_endpoints:
        if "s3ib" in endpoint:
            harvester_s3i_endpoint = endpoint

    """
    id and endpoint of sender(Sawmill) are known 
    """
    sender = "s3i:2073c475-fee5-463d-bce1-f702bb06f899"
    sender_endpoint = "s3ib://s3i:2073c475-fee5-463d-bce1-f702bb06f899"

    """
    Step 4: Prepare a service request, which will be sent to the harvester
    """
    service_request = prepare_service_request()

    """
    Step 6: Authentication with access token (JWT) at S3I Broker and listen to the callback function 
    """
    loop = asyncio.get_event_loop()
    broker = connect_to_broker(token=access_token, loop=loop)

    """
    Step 7: send the message
    """
    APP_LOGGER.info("Attempts to send a service request to harvester to retrieve the production data")
    broker.add_on_channel_open_callback(
        broker.send,
        True,
        [harvester_s3i_endpoint],
        json.dumps(service_request.base_msg)
    )

    """
    Step 8: Enable the loop 
    """
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        APP_LOGGER.info("Disconnect from S3I")
        loop.close()