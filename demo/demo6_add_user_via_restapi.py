from s3i import IdentityProvider, Config, Directory, TokenType
import json


if __name__ == "__main__":
    print("************************************************************")
    print("**Welcome to demo: add user via S3I Config REST API*********")
    print("************************************************************")
    """query access token of existing user. """
    username = input(
        "[S3I]: Please enter an existing username: ")
    password = input(
        "[S3I]: Please enter the corresponding password: ")
    idp_token = IdentityProvider(grant_type="password", client_id="admin-cli",
                                 client_secret="", realm="KWH", identity_provider_url="https://idp.s3i.vswf.dev/",
                                 username=username, password=password)
    token = idp_token.get_token(TokenType.ACCESS_TOKEN)
    print("[S3I]: token obtained")

    """username/password for creating new user in S3I"""
    create_username = input(
        "[S3I]: Please enter the selected username for new user: ")
    create_password = input(
        "[S3I]: Please enter the selected password for new user: ")

    """config instance"""
    s3i_config = Config(server_url="https://config.s3i.vswf.dev/", token=token)

    """step 1: create person in S3I."""

    create_person_res = s3i_config.create_person(
        username=create_username, password=create_password)
    dt_person_client_id = create_person_res.json()[
        "thingIdentity"]["identifier"]
    dt_person_client_secret = create_person_res.json()[
        "thingIdentity"]["secret"]
    person_uuid = create_person_res.json()["personIdentity"]["identifier"]

    print("[S3I]: Created a person named: {}".format(create_username))
    print("[S3I]: Created person has an identity (uuid): {}".format(person_uuid))
    print("[S3I]: Created DT for person has thing identity (uuid): {}".format(
        dt_person_client_id))
    print("[S3I]: Created DT for person has secret: {}".format(
        dt_person_client_secret))

    """step 2: log in with created user + PW in order to obtain access token from S3I IdP"""
    idp_token_new_user = IdentityProvider(grant_type="password",
                                          client_id=dt_person_client_id,
                                          client_secret=dt_person_client_secret,
                                          realm="KWH", identity_provider_url="https://idp.s3i.vswf.dev/",
                                          username=create_username,
                                          password=create_password)
    access_token = idp_token_new_user.get_token(TokenType.ACCESS_TOKEN)

    """config instance with access token entered"""
    s3i_new_user_config = Config(
        server_url="https://config.s3i.vswf.dev/", token=access_token)

    """step 3: create another thing (in this demo, an HMI of the new user) in S3I Dir"""
    create_thing_res = s3i_new_user_config.create_thing()
    dt_thing_client_id = create_thing_res.json()["identifier"]

    print("[S3I]: Created an HMI for person '{}'. This HMI has an identifier: {}".format(create_username,
                                                                                         dt_thing_client_id))

    """step 4: create an S³I-endpoint for the thing"""
    create_endpoint_res = s3i_new_user_config.create_broker_queue(thing_id=dt_thing_client_id, encrypted=False)
    print("[S3I]: HMI has an endpoint {}".format("s3ib://{}".format(dt_thing_client_id)))

    """step 5: update things in S3I Dir"""
    s3i_dir = Directory(
        s3i_dir_url="https://dir.s3i.vswf.dev/api/2/", token=access_token)

    """step 5.1: update the new HMI (add name, type...)"""
    hmi_body = s3i_dir.queryThingIDBased(dt_thing_client_id)
    hmi_body["attributes"]["name"] = "HMI of {}".format(create_username)
    hmi_body["attributes"]["type"] = "hmi"
    s3i_dir.updateThingIDBased(dt_thing_client_id, hmi_body)

    """step 5.2: update DT of the (new) person (add defaultHMI)"""
    dt_person_body = s3i_dir.queryThingIDBased(dt_person_client_id)
    dt_person_body["attributes"]["defaultHMI"] = dt_thing_client_id
    s3i_dir.updateThingIDBased(dt_person_client_id, dt_person_body)
