from s3i import IdentityProvider, Directory, BrokerAMQP, Key, UserMessage, TokenType, setup_logger, APP_LOGGER
import uuid
import json
import jwt
import base64
import os
import asyncio
from jwt import DecodeError, ExpiredSignatureError, InvalidTokenError
from jwt.algorithms import RSAPSSAlgorithm

schmitz_hmi_id = "s3i:6f58e045-fd30-496d-b519-a0b966f1ab01"
schmitz_hmi_secret = "475431fd-2c6d-4cae-bdfa-87226fff0cef"


def callback(ch, method, properties, body):
    if isinstance(body, bytes):
        uMsg = UserMessage(pgp_msg=body)

        if uMsg.pgp_msg.is_encrypted:
            """
            Decrypt the message and verify the signature
            """
            fp = os.path.dirname(os.path.abspath(__file__))
            fp = os.path.join(fp, "..", "key")
            pgpKey_sec = Key(path_demo_dir=fp, filename="sachverstaendiger_sec.asc")
            with pgpKey_sec.key.unlock("sachverstaendiger") as sec_key:
                uMsg.decrypt(sec_key, uMsg.pgp_msg)
                APP_LOGGER.info("Message decrypted")

            """
            Decompress the message
            """
            uMsg.convert_pgp_to_gzip()
            uMsg.decompress(uMsg.gzip_msg)
            APP_LOGGER.info("Message decompressed")

            """
            Verify the signature of the message 
            """
            sender = uMsg.base_msg.get("sender")
            s3i_directory = Directory(s3i_dir_url="https://dir.s3i.vswf.dev/api/2/", token=access_token)
            pub_key_sender = Key(key_str=s3i_directory.getPublicKey(sender)).key
            APP_LOGGER.info("The signature of the message is verified as {}".format(uMsg.verify(pub_key_sender,
                                                                                                uMsg.pgp_msg)))

            APP_LOGGER.info("Subject of the message: " + uMsg.base_msg["subject"])
            APP_LOGGER.info("Text of the message:  " + uMsg.base_msg["text"])

            attachments_list = uMsg.base_msg["attachments"]

            """
            store the attachment file in specified path
            """
            for i in range(len(attachments_list)):
                cwd = os.path.dirname(__file__)
                path = os.path.join(cwd, "demo_received_data",
                                    attachments_list[i]["filename"])
                file = open(path, 'wb')
                decode = base64.b64decode(attachments_list[i]["data"])
                file.write(decode)
                APP_LOGGER.info("Attachment " + uMsg.base_msg["attachments"][i]["filename"] + " of the message is stored in " + path)
                file.close()


def authenticate(username, password):
    idp = IdentityProvider(
        grant_type='password',
        identity_provider_url="https://idp.s3i.vswf.dev/",
        realm='KWH',
        client_id=schmitz_hmi_id,
        client_secret=schmitz_hmi_secret,
        username=username,
        password=password
    )
    APP_LOGGER.info("Username and password are sent to S3I IdentityProvider.")
    return idp


def get_token(idp):
    APP_LOGGER.info("Token obtained.")
    return idp.get_token(TokenType.ACCESS_TOKEN)


def verify_token(idp, token):
    idp_keys = idp.get_certs()["keys"]
    public_key = ""
    for key in idp_keys:
        if key["alg"] == "RS256" and key["kty"] == "RSA" and key["use"] == "sig":
            public_key = RSAPSSAlgorithm.from_jwk(json.dumps(key))
            break
    try:
        """decode the access token with public key that was searched above"""
        jwt.decode(
            jwt=token,
            key=public_key,
            algorithms=["RS256"],
            audience="rabbitmq"
        )
    except (DecodeError, ExpiredSignatureError, InvalidTokenError):
        return False
    else:
        return True


def connect_to_broker(token, loop):
    APP_LOGGER.info("Connecting to the broker endpoint at {}".format(hmi_schmitz_endpoint))
    br = BrokerAMQP(
        token=token,
        endpoint=hmi_schmitz_endpoint,
        callback=callback,
        loop=loop
    )
    br.connect()
    return br


if __name__ == "__main__":

    setup_logger("[S3I][Demo5][Receiver]")
    """
    Step 1: Receiver tries to authenticate himself against S3I IdentityProvider, in order to obtain an access token.
    In this Step, the grant type is selected as password which is a simply way to get a access token. 
    In a subsequent version, the grant type as authorization code flow will be implemented 
    """
    APP_LOGGER.info("Please log in as forest expert (Sachverstaendiger).")
    username = input('[S3I]: Please enter the username: \n')
    password = input('[S3I]: Please enter the password: \n')
    s3i_identity_provider = authenticate(username, password)
    access_token = get_token(s3i_identity_provider)

    """
    Step 2: Decode the access token
    """
    if verify_token(s3i_identity_provider, access_token):
        APP_LOGGER.info("Token is valid")
    else:
        APP_LOGGER.error("Invalid token")

    """
    The endpoint of schmitz's HMI is known
    """
    hmi_schmitz_endpoint = "s3ibs://s3i:6f58e045-fd30-496d-b519-a0b966f1ab01"

    """
    Step 3: Authentication with access token (JWT) at S3I Broker and listen to the callback function 
    """
    loop = asyncio.get_event_loop()
    broker = connect_to_broker(token=access_token, loop=loop)

    """
    Step 4: Enable the loop 
    """
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        APP_LOGGER.info("Disconnect from S3I")
        loop.close()
