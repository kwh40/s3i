from s3i import IdentityProvider, Directory, BrokerAMQP, ServiceRequest, ServiceReply, TokenType, APP_LOGGER, setup_logger, S3IBrokerAMQPError
import json
import uuid
import os
import base64
import jwt
from jwt import DecodeError, ExpiredSignatureError, InvalidTokenError
from jwt.algorithms import RSAPSSAlgorithm
import asyncio


def callback(ch, method, properties, body):
    """
    This function will be called and executed every time that S3I Broker delivers messages to the consuming application (HMI). \n
    Result contained in received service reply will be parsed and printed.

    Args:
        ch: channel used during consuming, which binds productor and consumer \n
        method: „method“ contains meta information regarding message delivery: It provides delivery flags, redelivery flags, and a routing key that can be used to acknowledge the message. \n
        properties: properties of queue and exchange which are used during consuming \n
        body: content of message
    """
    if isinstance(body, bytes):
        try:
            service_reply = ServiceReply(base_msg=body)
        except S3IBrokerAMQPError as e:
            APP_LOGGER.error(e)
        else:
            APP_LOGGER.info("Service reply: {}".format(service_reply.base_msg.get("results")))


def authenticate(username, password):
    idp = IdentityProvider(
        grant_type='password',
        identity_provider_url="https://idp.s3i.vswf.dev/",
        realm='KWH',
        client_id="s3i:6f58e045-fd30-496d-b519-a0b966f1ab01",
        client_secret="475431fd-2c6d-4cae-bdfa-87226fff0cef",
        username=username,
        password=password
    )
    APP_LOGGER.info("Username and password are sent to S3I IdentityProvider.")
    return idp


def get_token(idp):
    APP_LOGGER.info("Token obtained.")
    return idp.get_token(TokenType.ACCESS_TOKEN)


def verify_token(idp, token):
    idp_keys = idp.get_certs()["keys"]
    public_key = ""
    for key in idp_keys:
        if key["alg"] == "RS256" and key["kty"] == "RSA" and key["use"] == "sig":
            public_key = RSAPSSAlgorithm.from_jwk(json.dumps(key))
            break
    try:
        """decode the access token with public key that was searched above"""
        jwt.decode(
            jwt=token,
            key=public_key,
            algorithms=["RS256"],
            audience="rabbitmq"
        )
    except (DecodeError, ExpiredSignatureError, InvalidTokenError):
        return False
    else:
        return True


def search_service_in_dir(token):
    s3i_dir = Directory(
        s3i_dir_url="https://dir.s3i.vswf.dev/api/2/", token=token)
    cal_service = s3i_dir.queryAttributeBased(
        key='thingStructure/services/serviceType', value='calculateStock')
    service_id = cal_service[0]["thingId"]
    service_endpoints = cal_service[0]["attributes"]["allEndpoints"]
    APP_LOGGER.info("Service (id: {}) is found".format(service_id))
    return service_id, service_endpoints


def connect_to_broker(token, loop):
    APP_LOGGER.info("Connecting to the broker endpoint at {}".format(sender_endpoint))
    br = BrokerAMQP(
        token=token,
        endpoint=sender_endpoint,
        callback=callback,
        loop=loop
    )
    br.connect()
    return br


def prepare_service_request():
    file_name = input(
        '[S3I]: please enter the name of zipped ShapeFile (demo2.shp.zip): \n')

    parameters = dict()

    path_shapefile = os.path.join(os.path.dirname(
        __file__), "demo_send_data", file_name)

    with open(path_shapefile, "rb") as f:
        base64_data = base64.b64encode(f.read())
        data = base64_data.decode()

    parameters["surface"] = data
    msg_uuid = "s3i:" + str(uuid.uuid4())

    servReq = ServiceRequest()
    servReq.fillServiceRequest(
        sender=sender,
        receivers=[service_id],
        message_id=msg_uuid,
        service_type="calculateStock",
        reply_to_endpoint=sender_endpoint,
        parameters=parameters
    )

    return servReq


if __name__ == "__main__":
    setup_logger("[S3I][Demo2][Sender]")

    """
    Step 1: Sender tries to authenticate himself against S3I IdentityProvider, in order to obtain an access token.
    In this Step, the grant type is selected as password which is a simply way to get a access token. 
    In a subsequent version, the grant type as authorization code flow will be implemented 
    """
    APP_LOGGER.info("Please log in as forest expert (Sachverstaendiger).")
    username = input('[S3I]: Please enter the username: \n')
    password = input('[S3I]: Please enter the password: \n')
    s3i_identity_provider = authenticate(username, password)
    access_token = get_token(s3i_identity_provider)

    """
    Step 2: Decode the access token
    """

    if verify_token(s3i_identity_provider, access_token):
        APP_LOGGER.info("Token is valid")
    else:
        APP_LOGGER.error("Invalid token")

    """
    Step 3: Search the service id and its s3i endpoint in S3I Directory 
    """
    service_id, service_endpoints = search_service_in_dir(token=access_token)
    service_s3i_endpoint = ""
    for endpoint in service_endpoints:
        if "s3ib" in endpoint:
            service_s3i_endpoint = endpoint

    """
    The id and endpoint of sender's HMI are known  
    """
    sender = "s3i:6f58e045-fd30-496d-b519-a0b966f1ab01"
    sender_endpoint = "s3ibs://s3i:6f58e045-fd30-496d-b519-a0b966f1ab01"

    """
    Step 5: Authentication with access token (JWT) at S3I Broker
    """
    loop = asyncio.get_event_loop()
    broker = connect_to_broker(token=access_token, loop=loop)

    """
    Step 6: prepare the parameter data (ShapeFile)
    """
    service_request = prepare_service_request()

    """
    Step 7: send the message
    """
    APP_LOGGER.info("Attempt to send a serviceRequest to calculation service")
    broker.add_on_channel_open_callback(
        broker.send,
        True,
        [service_s3i_endpoint],
        json.dumps(service_request.base_msg)
    )

    """
    Step 8: Enable the loop 
    """
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        APP_LOGGER.info("Disconnect from S3I")
        loop.close()
