# Documentation
This package contains everything you need to get started with the S3I. The documentation of the S3I package and its demos can be found [here](https://kwh40.pages.rwth-aachen.de/s3i/).

# Installation
s3i is available on PyPI:

```
python -m pip install s3i
```

# Development
The s3i source code is hosted on [gitlab](https://git.rwth-aachen.de/kwh40/s3i). 

## Contributing
Feel free to contribute to this project. For more details on submitting changes see [CONTRIBUTING.md](./CONTRIBUTING.md)