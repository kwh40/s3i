*****************************
Demo 7 - S3I directory entry 
*****************************

Create and update a directory entry
====================================

In this demo, we demonstrate how to create a S3I Thing and and its empty directory entry using S3I Config API and update the entry using S3I Directory API. When running the Python script *demo7_add_directory_entry.py*, enter username and password of an existing user and its corresponding client id and secret(if you have none, contact us at s3i@kwh40.de). The process within this script consists of four steps:

Step 1: create a S3I Thing and its empty directory entry using S3I Config API 

Step 2: update the directory entry using S3I Directory API 

Step 3: delete the created Thing 

More details can be found in the Python script.

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Contents:

.. automodule:: demo.demo7_add_directory_entry
    :members:
