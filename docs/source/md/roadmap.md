# Roadmap

## June 2020:
* Integrate those things into the S3I that were shown at the presentation of the Smart Forest Lab on May 8, 2019. 
## Following:
* Add tests to the S³I Lib, which run as part of the continuous integration and ensure the functionality of the code.
* Introduce events with which users and applications can be notified by the S³I.
* Web interface for the convenient configuration, management and use of the S³I
