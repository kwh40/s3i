# REST APIs
## Communication via REST APIs

S3I's services can all be accessed using REST APIs. 

### S3I IdentityProvider

To get S3I IdentityProvider's public key (to validate the access token (JWT) it provides), you can use (without providing an authorisation header) a HTTP GET to:
```
"https://idp.s3i.vswf.dev/auth/realms/KWH/protocol/openid-connect/certs"
``` 
To get a JWT authorization token, this API can be used with a HTTP POST:
```
"https://idp.s3i.vswf.dev/auth/realms/KWH/protocol/openid-connect/token"
```
The request must use a body comprising:
```
{
	"grant_type": "password",
	"client_id": <your client id>,
	"client_secret": <your client secret>,
	"username": <your s3i username>,
	"password": <your s3i password>
}
```
When a person wants to get a token, its PersonIdentity (username and password) must be used. Additionally, the credentials of the ThingIdentity (client-id and -secret) of the HMI it uses must also be provided. If no HMI is in use or available, "admin-cli" must be provided as client-id and the client-secret can be ignored.

Besides the body, a header has to be specified:
```
{"Content-Type": "application/x-www-form-urlencoded"}
```

More details of the REST API of S3I IdentityProvider's underlying Keycloak can be found here: [IdP Admin REST API](https://www.keycloak.org/docs-api/5.0/rest-api/index.html#_overview).

Use the [web interface](https://idp.s3i.vswf.dev/auth/realms/KWH/account/) to manage your user account. Through the web interface, you can change your password, manage your account data, choose an authorization method and see all applications within your realm.

### S3I Directory

S3I Directory provides a REST API to query and update things and policies. It is available at:
```
"https://dir.s3i.vswf.dev/api/2/things/<thing_id>"
```
A comprehensive documentation can be found at [Dir REST API](https://dir.s3i.vswf.dev/apidoc/). Note, however, that only querying and updating existing entries is supported by S3I Directory. Completely new entries are created using S3I Config (see below).

Every REST call must comprise an authorization bearer header with an access token (JWT):
```
{
	"Content-Type": "application/json", 
	"Authorization": "Bearer <access token>"
}
```

#### Example 1: Find Harvester thing based on its identifier s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63
```
HTTP request: GET
url: "https://dir.s3i.vswf.dev/api/2/things/s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63"
body: empty
header:{
	"Content-Type": "application/json", 
	"Authorization": "Bearer <access token>"
}

HTTP response 
status code: 200,
response body: 
{
    "thingId": "s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63",
    "policyId": "s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63",
    "attributes": {
        "ownedBy": "606d8b38-4c3f-46bd-9482-86748e108f32",
        "name": "Harvester",
        "location": {
            "longitude": 10.117,
            "latitude": 20.136
        },
        "publicKey": "...",
        "type": "component",
        "dataModel": "fml40",
        "allEndpoints": [...],
        "thingStructure": {
            "class": "fml40.Harvester",
            "services": [...],
            "links": [...]
        }
    }
}

```
#### Example 2: Get location of Harvester:
```
HTTP request: GET
url: "https://dir.s3i.vswf.dev/api/2/things/s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63/attributes/location"
body: empty
header: {
	"Content-Type": "application/json", 
	"Authorization": "Bearer <access token>"
}

HTTP response 
status code: 200,
response body: 
{
    "latitude": 10.117,
    "longitude": 20.136
}
```
Example 3: Update location of this Harvester:
```
HTTP request: PUT
url: "https://dir.s3i.vswf.dev/api/2/things/s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63/attributes/location"
body: 
{
    "latitude": 21.221,
    "longitude": 10.1852
}
header: 
{
	"Content-Type": "application/json", 
	"Authorization": "Bearer <access token>"
}

HTTP response
status code: 204
```

### S3I Broker

The S3I Broker can be accessed using the S3I Python Library or the broker's REST API. The REST API offers a convenient access to the Broker allowing to send and receive S3I-B messages without using the AMQP protocol. The Broker REST API is available at: 

[https://broker.s3i.vswf.dev/apidoc/](https://broker.s3i.vswf.dev/apidoc/)

### S3I Config

For a coordinated creation and deletion of persons and things within all of S3I's services, an additional S3I Config REST API is provided. A corresponding documentation for this API is available at: 

[https://config.s3i.vswf.dev/apidoc/](https://config.s3i.vswf.dev/apidoc/)

### S3I Repository

An interactive documentation of S3I Repository's REST API can be found here (make sure to switch to server "/api/2 - local Ditto"):

[https://ditto.s3i.vswf.dev/apidoc/](https://ditto.s3i.vswf.dev/apidoc/)
