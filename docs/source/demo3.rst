***************************
Demo 3 - DT to DT
***************************

Requesting production data from a forest machine
================================================

In demo 3, a saw mill (actually, its Digital Twin (DT)) sends a request for production data to a forest machine (its DT) using an S3I-B service request and reply. Run the following Python scripts to replay this demo:

1.	demo3_forest_machine.py starts a listener that waits for messages from the sender until it is stopped manually. It simulates a forest machine’s service that returns the requested production data via an S3I-B service reply message.
  
2.	demo3_sawmill.py sends an S3I-B service request to the forest machine and then waits for the reply. Subsequently, the reply message with the production data is received and displayed.  
   

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Contents:

.. automodule:: demo.demo3_forest_machine
    :members:
   
.. automodule:: demo.demo3_sawmill
    :members:
