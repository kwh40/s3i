*****************************
Demo 8 - S3I repository entry 
*****************************

Create and update a repository entry
====================================

In this demo, we demonstrate how to create a S3I Thing and and its empty repository entry using S3I Config API and update the entry using S3I Repository API. When running the Python script *demo8_add_repository_entry.py*, enter username and password of an existing user and its corresponding client id and secret(if you have none, contact us at s3i@kwh40.de). The process within this script consists of four steps:

Step 1: create a S3I Thing and its optional repository entry using S3I Config API 

Step 2: update the repository entry using S3I Repository API 

Step 3: delete the created Thing 

More details can be found in the Python script.

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Contents:

.. automodule:: demo.demo8_add_repository_entry
    :members:
