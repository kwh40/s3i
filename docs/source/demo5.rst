***************************
Demo 5 - Compressed Messages
***************************

Compressed message exchange between persons
==========================================

Similar to the scenario in demo 1, a forest owner (sender) sends an S3I-B user message to a forest expert (receiver). In this new demo, the message will be compressed using GZIP format.

1.	demo5_receiver_hmi.py simulates the receiver’s human machine interface (HMI), e.g., an app. It starts a listener waiting for messages from the sender until it is stopped manually.
   •	Enter username/password for forest expert
2.	demo5_sender_hmi.py simulates the sender’s HMI and sends encrypted and signed messages to the receiver
   •	Enter username/password for forest owner
   •	Enter subject and text
   •  Optional: Enter the file names of attachments which are located in the folder demo_send_data (space seperated list)


.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Contents:

.. automodule:: demo.demo5_receiver_hmi
    :members:

.. automodule:: demo.demo5_sender_hmi
    :members:
