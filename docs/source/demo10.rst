*********************
Demo 10 - Wheel Loader
*********************

In this set of demos, a wheel loader's Digital Twin (DT), an asset from KWH4.0's gamified Smart Forest Lab (gSFL), can now be accessed using S3I. It runs on a Raspberry Pi on KWH4.0's premises and provides different means of interaction as follows. *Note* that passwords are required in order to use the private gpgs key.

Service Request
=====================
In this demo, a service request is sent from a forest expert's HMI to the DT of the wheel loader, see *demo10_serviceRequest_wheelLoader.py*.

Get Value Request
=============================
Besides service calls, S3I Broker now allows for special requests to directly access a DT's data (particularly, for DTs running on edge devices). With the *get value request* message, you can request a specific value from the wheel loader specified by an attribute path. The data structure can be derived from the general ml40/fml40 data model or you do not specify any attribute path so that all data of the wheel loader is received. The attribute path should contain the keys which are given in the JSON representation of the data model. If a (JSON) list follows a key, you first have to indicate the class of the next entry. If the entry is of class *ml40::Thing*, you have to indicate the role (ml40::Role) of that thing. For example, the fml40 JSON structure of the wheel loader looks like this: 

.. literalinclude:: json/dataModel_wheelloader.json
   :language: JSON

If you want to get the *currentWeight* of the *LogLoadingArea*, the attribute path would be: *attributes/features/ml40::Composite/targets/fml40::LogLoadingArea/features/ml40::Weight/currentWeight*. 

The get value reply message contains a list with the name of the thing first, followed by the key and the requested value itself. For the given examples, the response looks like this: ['LoadingArea', 'currentWeight', '2356']. 

In *demo10_getValueRequest_wheelLoader.py*, a get value request is sent to the gSFL wheel loader. 

REST Interface to S3I Repository
============================================
The gSFL wheel loader's Digital Twin not only resides on said Edge device (Raspberry Pi), but also stores a copy of itself in the S3I Repository, a Ditto-based cloud storage provided by KWH4.0. It can be accessed using its REST interface. A convenient web interface is provided `here <https://ditto.s3i.vswf.dev/apidoc>`_. Make sure you you choose */api/2 -local Ditto* as your server to access S3I Repository. Currently, all S3I clients have the permission to observe the wheel loader.

Websocket Connection to S3I Repository
============================================
Another way to access this cloud copy of the wheel loader's DT in S3I Repository is using websocket connections. A websocket connection is a bi-directional connection, thus, there is no need for polling for changes as with REST.

Whenever this cloud DT's data changes, a `Ditto event <https://www.eclipse.org/ditto/basic-signals-event.html>`_ is created by S3I Repository. This event is populated to every client with permission to read the updated feature and is registered to receive such events. To register, *START-SEND-EVENTS* needs to be sent to S3I Repository. To only receive a selection of events, a filter can be added. 
This filter must be in `RQL-Style <https://www.eclipse.org/ditto/basic-rql.html>`_. A simple filter command contains the *CLIENT_ID*, the *FILTER_EXPRESSION* like "eq" (for equals to), "lt" (for lower than), "gt" (for greater than) [...], the *FEATURE_PATH* and the *COMPARE_VALUE*. 
The final message looks like this: *START-SEND-EVENT?namespaces=CLIENT_ID&filter=FILTER_EXPRESSION(FEATURE_PATH,COMPARE_VALUE)*.

Additionally, you can send Ditto messages from one to another client via S3I Repository. For more information see this `documentation <https://www.eclipse.org/ditto/basic-messages.html#elements>`_. Note, however, that these messages are different from S3I Broker messages!

The communication and event handling is demonstrated in *demo10_wheelloader_monitoring_ws.py*.

