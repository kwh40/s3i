.. S3I documentation master file, created by
   sphinx-quickstart on Tue Dec 17 15:28:08 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Welcome to the S3I documentation!
=================================
This is a documentation of the Smart Systems Service Infrastructure (S3I) by Kompetenzzentrum Wald und Holz 4.0 (KWH4.0). It comprises some preliminaries for installation, the S3I Python package (`Gitlab <https://git.rwth-aachen.de/kwh40/s3i>`_), an overview of S3I's REST APIs, a list of demo things, and several S3I demo scenarios provided as Python sample code. The S3I toolkit is used for basic interaction with S3I. The required 3rd party Python libraries are listed in requirements.txt. Please contact us (s3i@kwh40.de) for the username + password of the users mentioned in the demo scenarios ("forest expert" and "forest owner") and the the passwords of their private gpg keys. You can also contact us for a personal user account or do the registration with the `S3I Manager <https://manager.s3i.vswf.dev/>`_ . Please do not hesitate to contact us for any question regarding S3I as well.
Demo 1-3 correspond to the three use cases described in KWH4.0’s `position paper <https://www.kwh40.de/wp-content/uploads/2020/04/KWH40-Standpunkt-S3I-v2.0.pdf>`_ on S3I (currently in German only). This paper also describes the concepts of S3I.

An additional documentation is provided to help accessing S3I services without programming knowledge. The documentation is available under this `Link <https://git.rwth-aachen.de/kwh40/s3i/-/blob/master/docs/howto/HOWTO-%20S3I%20without%20programming%20knowledge.pdf>`_


Table of Contents
-----------------

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Contents:
   
   md/preliminaries.md
   S3I-Package
   md/communication.md
   demo1
   demo2
   demo3
   demo4
   demo5
   demo6
   demo7
   demo8
   demo9
   demo10
   md/things.md


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

