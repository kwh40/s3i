# System requirements
This repository is managed with [poetry](https://python-poetry.org/docs/). Make sure you have it installed on your system. Installation instraction can be found [here](https://python-poetry.org/docs/#system-requirements). Also [git](https://git-scm.com/) should be installed on your system.

# Getting the code
Clone the s3i repository using git:

```
git clone https://git.rwth-aachen.de/kwh40/s3i
cd s3i
```

Install reqiured project dependencies:

```
pip install --upgrade pip 
poetry install
```

# Testing
This project uses pytest for managing tests. The entire test environemnt is executed with:
```
poetry run pytest 
```
## Test coverage
This package also supports the calculation of test coverage. To recaculate the metrics run

```
poetry run coverage run -m pytest tests
poetry run coverage report
```